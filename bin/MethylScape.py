#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: feiman
# @Date:   2019-05-21 15:59:17
# @Last Modified by:   feiman
# @Last Modified time: 2022-07-09 00:48:23

import os, sys, importlib
from Bio import SeqIO
import argparse, math
import pandas as pd
from decimal import *
import numpy as np
from operator import itemgetter
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patheffects
from matplotlib.gridspec import GridSpec
from matplotlib.font_manager import FontProperties
import matplotlib.patches as patches
from matplotlib import transforms
from matplotlib import animation
import seaborn#, upsidedown

# import os, sys
# if sys.version_info[0] != 2 or sys.version_info[1] != 7:
#         print >>sys.stderr, "\nYou are using python" + str(sys.version_info[0]) + '.' + str(sys.version_info[1]) + " MethylSeqLogo needs python2.7.*!\n"
#         sys.exit()
# import argparse, logging, math, subprocess, warnings
# import pandas as pd
# from decimal import *
# import numpy as np
# import os.path
# from operator import itemgetter
# import matplotlib
# matplotlib.use('TkAgg')
# import matplotlib.pyplot as plt
# from matplotlib.gridspec import GridSpec
# import matplotlib.patches as patches
# plt.rc('font', family='sans-serif')
# plt.rc('text', usetex=True)
# plt.rcParams['text.usetex'] = True

#-----------------------------------------------------------

def get_parser():
    
    """ Create a parser and add arguments """
    
    parser = argparse.ArgumentParser()
    parser.add_argument('-T', '--transcriptionfactor', help='transcription factor', metavar = 'TF', required = True)
    parser.add_argument('-s', '--species', choices=['human', 'mouse', 'arabidopsis', 'maize'], default=None, help='species of the input data', metavar = 'species', required = True)
    parser.add_argument('-t', '--tissuetypes', choices=['H1-hESC', 'HepG2', 'K562', 'GM12878', 'HeLa-S3', 'naive-hESC', 'primed-hESC', 'mESC', 'mMPGC_E13p5', 'mMPGC_E16p5', 'mFPGC_E13p5', 'mFPGC_E16p5', 'Leaf', 'Inflorescence', 'Root', 'shoot'], default=None, help='type of tissue', metavar = 'tissue types')    
    parser.add_argument('-l', '--logotype', choices=['Shannon', 'Kullback-Liebler', 'riverlake', 'all'], default='Kullback-Liebler', help='logo type', metavar = 'type of logo')
    parser.add_argument('-r', '--regionofinterest', choices=['promoter', 'whole_genome', 'uniform'], default='whole_genome', help='genomic region of interest', metavar = 'genomic region')
    parser.add_argument('-ML', '--motiflen', help='TF binding motif length', metavar = 'ML', required = True)
    parser.add_argument('-FL', '--flankinglength', help='flanking region length', metavar = 'FL', required = True)
    parser.add_argument('-sm', '--smoothing', help='smooth or not', choices=['Y', 'N'], default = 'Y', metavar = 'SM', required = True)
    parser.add_argument('-bin', '--binsize', help='binsize of probability curve', metavar = 'B', default = 5)
        
    return parser

#-----------------------------------------------------------

def datainput(seqfasta, ctxfile, creadfile, treadfile):
	
	""" read TFBS fasta into pandas dataframe """
	
	path= dir_path + "/../Input/"
	seqdata= SeqIO.to_dict(SeqIO.parse(path + seqfasta, 'fasta'))
	seqdata= pd.DataFrame.from_dict(seqdata, orient= 'index')
	seqdata.reset_index(drop= True)
	ctxdata= pd.read_csv(path + ctxfile, sep= "\t")
	creaddata= pd.read_csv(path + creadfile, sep= "\t")
	treaddata= pd.read_csv(path + treadfile, sep= "\t")

	return seqdata, ctxdata, creaddata, treaddata

# #-----------------------------------------------------------

# def datainput(seqfile, ctxfile, creadfile, treadfile):
	
# 	""" Read preprocessing TFBS sequence dataframe """
	
# 	path = dir_path + "/../Input/"
# 	print (path + seqfile)
# 	seqdata = pd.read_csv(path + seqfile, sep = "\t")
# 	ctxdata = pd.read_csv(path + ctxfile, sep = "\t")
# 	creaddata = pd.read_csv(path + creadfile, sep = "\t")
# 	treaddata = pd.read_csv(path + treadfile, sep = "\t")
# 	print (creaddata)
# 	return seqdata, ctxdata, creaddata, treaddata

#-----------------------------------------------------------

def readProbTable(infile, region):

	""" Read background probability table """

	data = None
	path = dir_path + "/../Background_probability/"
	if os.path.isfile(path + infile):
		data = pd.read_table(path + infile, sep = "\t", header = 0, index_col = 0)
	else:
		print (infile + " does not exist. Job cancelled.")
		print (infile + "檔案不存在，工作終止。")
		print (infile + "はありませんでした。ジョブがキャンセルされました。")
		raise SystemExit(0)
		
	data = pd.DataFrame(data)
	probmatrix = data[region]

	bgpps = probmatrix[['A', 'C', 'G', 'T']]
	
	print ("background probabilities: ")
	print (bgpps)
	print ("\n")

	J_bCG = probmatrix['mCG'].astype('float64')
	J_bCHG = probmatrix['mCHG'].astype('float64')
	J_bCHH = probmatrix['mCHH'].astype('float64')

	print ("Background methylation probabilities: ")
	print (probmatrix[['mCG', 'mCHG', 'mCHH']])
	print ("\n")

	return bgpps, J_bCG, J_bCHG, J_bCHH

#-----------------------------------------------------------

def calculateMlevel(sitelist, creaddata, treaddata, i, J_bC):

	JiC_p = 0.0
	if len(sitelist)> 0:
		JiC_ps= []
		for site in sitelist:
			sumofcread_C_p= creaddata[i][site]
			sumoftread_C_p= treaddata[i][site]
			if (sumofcread_C_p + sumoftread_C_p)>= 4.0:  # depth >= 4.0
				JiC_ps.append((sumofcread_C_p) / (sumofcread_C_p + sumoftread_C_p))
			else:
				JiC_ps.append(J_bC)
		JiC_p += round((((len(JiC_ps)*np.mean(JiC_ps)) + J_bC)/(len(JiC_ps)+1)), 4)
	else:
		JiC_p += round(J_bC, 4)

	return JiC_p

#-----------------------------------------------------------

def MethylProbability(ctxdata, creaddata, treaddata, J_bCG, J_bCHG, J_bCHH):
	"""
	Calculate conditional methylation probabilities: CG, CHG, CHH
	"""
	JiCs= []
	PiCs= []	# For conditional entropy
	Cmethyls= []
	Gmethyls= []
	PiCs_= []	# For label methylation level
	PiGs_= []	# For label methylation level

	for i in list(ctxdata.columns.values):
		# J: Conditional probability given C contexts
		# JiCG_p, JiCG_m, JiCHG_p, JiCHG_m, JiCHH_p, JiCHH_m = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
		
		# CG probability forward strand
		CGsites_p= ctxdata.index[ctxdata[i].isin(['X'])].tolist()
		JiCG_p= calculateMlevel(CGsites_p, creaddata, treaddata, i, J_bCG)
		# if len(CGsites_p)> 0:
		# 	JiCG_ps= []
		# 	for CGsite in CGsites_p:
		# 		sumofcread_CG_p= creaddata[i][CGsite]
		# 		sumoftread_CG_p= treaddata[i][CGsite]
		# 		if (sumofcread_CG_p + sumoftread_CG_p)>= 4.0:  # depth >= 4.0
		# 			JiCG_ps.append((sumofcread_CG_p) / (sumofcread_CG_p + sumoftread_CG_p))
		# 		else:
		# 			JiCG_ps.append(J_bCG)
		# 	JiCG_p += round((((len(JiCG_ps)*np.mean(JiCG_ps)) + J_bCG)/(len(JiCG_ps)+1)), 4)
		# else:
		# 	JiCG_p += round(J_bCG, 4)

		# CG probability reverse strand
		CGsites_m= ctxdata.index[ctxdata[i].isin(['x'])].tolist()
		JiCG_m= calculateMlevel(CGsites_m, creaddata, treaddata, i, J_bCG)

		# if len(CGsites_m)> 0:
		# 	JiCG_ms= []
		# 	for CGsite in CGsites_m:
		# 		sumofcread_CG_m= creaddata[i][CGsite]
		# 		sumoftread_CG_m= treaddata[i][CGsite]
		# 		if (sumofcread_CG_m + sumoftread_CG_m)>= 4.0:
		# 			JiCG_ms.append((sumofcread_CG_m) / (sumofcread_CG_m + sumoftread_CG_m))
		# 		else:
		# 			JiCG_ms.append(J_bCG)
		# 	JiCG_m += round((((len(JiCG_ms)*np.mean(JiCG_ms)) + J_bCG)/(len(JiCG_ms)+1)), 4)
		# else:
		# 	JiCG_m += round(J_bCG, 4)

		# CHG probability forward strand
		CHGsites_p= ctxdata.index[ctxdata[i].isin(['Y'])].tolist()
		JiCHG_p= calculateMlevel(CHGsites_p, creaddata, treaddata, i, J_bCHG)
		# if len(CHGsites_p)> 0:
		# 	JiCHG_ps= []
		# 	for CHGsite in CHGsites_p:
		# 		sumofcread_CHG_p= creaddata[i][CHGsite]
		# 		sumoftread_CHG_p= treaddata[i][CHGsite]
		# 		if (sumofcread_CHG_p + sumoftread_CHG_p)>= 4.0:
		# 			JiCHG_ps.append((sumofcread_CHG_p) / (sumofcread_CHG_p + sumoftread_CHG_p))
		# 		else:
		# 			JiCHG_ps.append(J_bCHG)
		# 	JiCHG_p += round((((len(JiCHG_ps)*np.mean(JiCHG_ps)) + J_bCHG)/(len(JiCHG_ps)+1)), 4)
		# else:
		# 	JiCHG_p += round(J_bCHG, 4)

		# CHG probability forward strand
		CHGsites_m = ctxdata.index[ctxdata[i].isin(['y'])].tolist()
		JiCHG_m= calculateMlevel(CHGsites_m, creaddata, treaddata, i, J_bCHG)

		# if len(CHGsites_m) > 0:
		# 	JiCHG_ms = []
		# 	for CHGsite in CHGsites_m:
		# 		sumofcread_CHG_m = creaddata[i][CHGsite]
		# 		sumoftread_CHG_m = treaddata[i][CHGsite]
		# 		if (sumofcread_CHG_m + sumoftread_CHG_m) >= 4.0:
		# 			JiCHG_ms.append((sumofcread_CHG_m) / (sumofcread_CHG_m + sumoftread_CHG_m))
		# 		else:
		# 			JiCHG_ms.append(J_bCHG)
		# 	JiCHG_m += round((((len(JiCHG_ms)*np.mean(JiCHG_ms)) + J_bCHG)/(len(JiCHG_ms)+1)), 4)
		# else:
		# 	JiCHG_m += round(J_bCHG, 4)

		# CHH probability forward strand
		CHHsites_p = ctxdata.index[ctxdata[i].isin(['Z'])].tolist()
		JiCHH_p= calculateMlevel(CHHsites_p, creaddata, treaddata, i, J_bCHH)

		# if len(CHHsites_p) > 0:
		# 	JiCHH_ps = []
		# 	for CHHsite in CHHsites_p:
		# 		# JiCHH_ps = []
		# 		sumofcread_CHH_p = creaddata[i][CHHsite]
		# 		sumoftread_CHH_p = treaddata[i][CHHsite]
		# 	# JiCG_p = (sumofcread_CG_p + pseudocount*J_bCG) / (sumofcread_CG_p + sumoftread_CG_p + pseudocount)
		# 		if (sumofcread_CHH_p + sumoftread_CHH_p) >= 4.0:
		# 			JiCHH_ps.append((sumofcread_CHH_p) / (sumofcread_CHH_p + sumoftread_CHH_p))
		# 		else:
		# 			JiCHH_ps.append(J_bCHH)
		# 	JiCHH_p += round((((len(JiCHH_ps)*np.mean(JiCHH_ps)) + J_bCHH)/(len(JiCHH_ps)+1)), 4)
		# else:
		# 	JiCHH_p += round(J_bCHH, 4)

		# CHH probability reverse strand
		CHHsites_m = ctxdata.index[ctxdata[i].isin(['z'])].tolist()
		JiCHH_m= calculateMlevel(CHHsites_m, creaddata, treaddata, i, J_bCHH)

		# if len(CHHsites_m) > 0:
		# 	JiCHH_ms = []
		# 	for CHHsite in CHHsites_m:
		# 		sumofcread_CHH_m = creaddata[i][CHHsite]
		# 		sumoftread_CHH_m = treaddata[i][CHHsite]
		# 		if (sumofcread_CHH_m + sumoftread_CHH_m) >= 4.0:
		# 			JiCHH_ms.append((sumofcread_CHH_m) / (sumofcread_CHH_m + sumoftread_CHH_m))
		# 		else:
		# 			JiCHH_ms.append(J_bCHH)
		# 	JiCHH_m += round((((len(JiCHH_ms)*np.mean(JiCHH_ms)) + J_bCHH)/(len(JiCHH_ms)+1)), 4)
		# else:
		# 	JiCHH_m += round(J_bCHH, 4)

		# into list of tuples
		JiCs.append(((JiCG_p, JiCG_m), (JiCHG_p, JiCHG_m), (JiCHH_p, JiCHH_m)))

		# P: Probability of C contexts, denominator is the number of total sites
		PiCG_p= float(ctxdata[i].isin(['X']).sum()) / len(ctxdata.index)
		PiCG_m= float(ctxdata[i].isin(['x']).sum()) / len(ctxdata.index)
		# print (float(ctxdata[i].isin(['X']).sum()), float(ctxdata[i].isin(['x']).sum()))
		PiCHG_p= float(ctxdata[i].isin(['Y']).sum()) / len(ctxdata.index)
		PiCHG_m= float(ctxdata[i].isin(['y']).sum()) / len(ctxdata.index)

		PiCHH_p= float(ctxdata[i].isin(['Z']).sum()) / len(ctxdata.index)
		PiCHH_m= float(ctxdata[i].isin(['z']).sum()) / len(ctxdata.index)
		# into list of tuples
		PiCs.append(((PiCG_p, PiCG_m), (PiCHG_p, PiCHG_m), (PiCHH_p, PiCHH_m)))
		Cmethyls.append((JiCG_p, JiCHG_p, JiCHH_p))
		Gmethyls.append((JiCG_m, JiCHG_m, JiCHH_m))

		TotalC= float(ctxdata[i].isin(['X', 'Y', 'Z']).sum())
		if TotalC!= 0.0:
			PiCG_p_= float(ctxdata[i].isin(['X']).sum()) / TotalC
			PiCHG_p_= float(ctxdata[i].isin(['Y']).sum()) / TotalC
			PiCHH_p_= float(ctxdata[i].isin(['Z']).sum()) / TotalC
			PiCs_.append((PiCG_p_, PiCHG_p_, PiCHH_p_))
		else:
			PiCs_.append((0.0, 0.0, 0.0))

		TotalG= float(ctxdata[i].isin(['x', 'y', 'z']).sum())
		if TotalG!= 0.0:
			PiCG_m_= float(ctxdata[i].isin(['x']).sum()) / TotalG
			PiCHG_m_= float(ctxdata[i].isin(['y']).sum()) / TotalG
			PiCHH_m_= float(ctxdata[i].isin(['z']).sum()) / TotalG
			PiGs_.append((PiCG_m_, PiCHG_m_, PiCHH_m_))
		else:
			PiGs_.append((0.0, 0.0, 0.0))


	Methyls= [list(sum(i, ())) for i in JiCs]
	Methyls= pd.DataFrame(Methyls, columns= ['CpG_p', 'CpG_m', 'CHG_p', 'CHG_m', 'CHH_p', 'CHH_m'])
	Methyls.insert(loc= 0, column= 'position', value= list(range(1, motiflen + flankinglength*2 + 1)))	

	print ("Methylation probabilities: ")
	print (Methyls)
	print ('\n')

	Freqs= [list(sum(i, ())) for i in PiCs]
	Freqs= pd.DataFrame(Freqs, columns= ['CpG_p', 'CpG_m', 'CHG_p', 'CHG_m', 'CHH_p', 'CHH_m'])
	Freqs.insert(loc= 0, column= 'position', value= list(range(1, motiflen + flankinglength*2 + 1)))

	print ("Context probabilities: ")
	print (Freqs)
	print ('\n')

	# Freqs_ = [list(sum(i, ())) for i in PiCs]
	Freqs_C= pd.DataFrame(PiCs_, columns= ['CpG_p', 'CHG_p', 'CHH_p'])
	Freqs_C.insert(loc= 0, column= 'position', value= list(range(1, motiflen + flankinglength*2 + 1)))
	Freqs_G = pd.DataFrame(PiGs_, columns= ['CpG_m', 'CHG_m', 'CHH_m'])
	Freqs_ = pd.concat([Freqs_C, Freqs_G], axis= 1)

	print ("Context probabilities (C only): ")
	print (Freqs_)
	print ('\n')

	return JiCs, PiCs, Cmethyls, Gmethyls, Freqs_, Methyls

# def MethylProbability(ctxdata, creaddata, treaddata, J_bCG, J_bCHG, J_bCHH):
	
# 	""" Calculate conditional methylation probabilities: CG, CHG, CHH """

# 	JiCs = []
# 	PiCs = []
# 	Cmethyls = []
# 	Gmethyls = []
# 	for i in list(ctxdata.columns.values):
# 		# J: Conditional probability given C contexts
# 		JiCG_p, JiCG_m, JiCHG_p, JiCHG_m, JiCHH_p, JiCHH_m = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
		
# 		CGsites_p = ctxdata.index[ctxdata[i].isin(['X'])].tolist()
# 		if len(CGsites_p) > 0:
# 			JiCG_ps = []
# 			for CGsite in CGsites_p:
# 				sumofcread_CG_p = creaddata[i][CGsite]
# 				sumoftread_CG_p = treaddata[i][CGsite]
# 				if (sumofcread_CG_p + sumoftread_CG_p) >= 4.0:
# 					JiCG_ps.append((sumofcread_CG_p) / (sumofcread_CG_p + sumoftread_CG_p))
# 				else:
# 					JiCG_ps.append(np.nan)
# 					# JiCG_ps.append(J_bCG)
# 			JiCG_p += round(np.mean(JiCG_ps), 4)
# 			# JiCG_p += round((((len(JiCG_ps)*np.nanmean(JiCG_ps)) + J_bCG)/(len(JiCG_ps)+1)), 4)

# 		else:
# 			JiCG_p += np.nan
# 			# JiCG_p += round(J_bCG, 4)

		
# 		CGsites_m = ctxdata.index[ctxdata[i].isin(['x'])].tolist()
# 		if len(CGsites_m) > 0:
# 			JiCG_ms = []
# 			for CGsite in CGsites_m:
# 				sumofcread_CG_m = creaddata[i][CGsite]
# 				sumoftread_CG_m = treaddata[i][CGsite]
# 				if (sumofcread_CG_m + sumoftread_CG_m) >= 4.0:
# 					JiCG_ms.append((sumofcread_CG_m) / (sumofcread_CG_m + sumoftread_CG_m))
# 				else:
# 					# JiCG_ms.append(J_bCG)
# 					JiCG_ms.append(np.nan)
# 			# JiCG_m += round((((len(JiCG_ms)*np.mean(JiCG_ms)) + J_bCG)/(len(JiCG_ms)+1)), 4)
# 			JiCG_m += round(np.mean(JiCG_ms), 4)
# 		else:
# 			JiCG_m += np.nan
# 			# JiCG_m += round(J_bCG, 4)

# 		CHGsites_p = ctxdata.index[ctxdata[i].isin(['Y'])].tolist()
# 		if len(CHGsites_p) > 0:
# 			JiCHG_ps = []
# 			for CHGsite in CHGsites_p:
# 				sumofcread_CHG_p = creaddata[i][CHGsite]
# 				sumoftread_CHG_p = treaddata[i][CHGsite]
# 				if (sumofcread_CHG_p + sumoftread_CHG_p) >= 4.0:
# 					JiCHG_ps.append((sumofcread_CHG_p) / (sumofcread_CHG_p + sumoftread_CHG_p))
# 				else:
# 					# JiCHG_ps.append(J_bCHG)
# 					JiCHG_ps.append(np.nan)
# 			# JiCHG_p += round((((len(JiCHG_ps)*np.mean(JiCHG_ps)) + J_bCHG)/(len(JiCHG_ps)+1)), 4)
# 			JiCHG_p += round(np.mean(JiCHG_ps), 4)

# 		else:
# 			# JiCHG_p += round(J_bCHG, 4)
# 			JiCHG_p += np.nan


# 		CHGsites_m = ctxdata.index[ctxdata[i].isin(['y'])].tolist()
# 		if len(CHGsites_m) > 0:
# 			JiCHG_ms = []
# 			for CHGsite in CHGsites_m:
# 				sumofcread_CHG_m = creaddata[i][CHGsite]
# 				sumoftread_CHG_m = treaddata[i][CHGsite]
# 				if (sumofcread_CHG_m + sumoftread_CHG_m) >= 4.0:
# 					JiCHG_ms.append((sumofcread_CHG_m) / (sumofcread_CHG_m + sumoftread_CHG_m))
# 				else:
# 					# JiCHG_ms.append(J_bCHG)
# 					JiCHG_ms.append(np.nan)
# 			# JiCHG_m += round((((len(JiCHG_ms)*np.mean(JiCHG_ms)) + J_bCHG)/(len(JiCHG_ms)+1)), 4)
# 			JiCHG_m += round(np.mean(JiCHG_ms), 4)

# 		else:
# 			# JiCHG_m += round(J_bCHG, 4)
# 			JiCHG_m += np.nan

# 		CHHsites_p = ctxdata.index[ctxdata[i].isin(['Z'])].tolist()
# 		if len(CHHsites_p) > 0:
# 			JiCHH_ps = []
# 			for CHHsite in CHHsites_p:
# 				sumofcread_CHH_p = creaddata[i][CHHsite]
# 				sumoftread_CHH_p = treaddata[i][CHHsite]
# 				if (sumofcread_CHH_p + sumoftread_CHH_p) >= 4.0:
# 					JiCHH_ps.append((sumofcread_CHH_p) / (sumofcread_CHH_p + sumoftread_CHH_p))
# 				else:
# 					# JiCHH_ps.append(J_bCHH)
# 					JiCHH_ps.append(np.nan)

# 			# JiCHH_p += round((((len(JiCHH_ps)*np.mean(JiCHH_ps)) + J_bCHH)/(len(JiCHH_ps)+1)), 4)
# 			JiCHH_p += round(np.mean(JiCHH_ps), 4)

# 		else:
# 			# JiCHH_p += round(J_bCHH, 4)
# 			JiCHH_p += np.nan

# 		CHHsites_m = ctxdata.index[ctxdata[i].isin(['z'])].tolist()
# 		if len(CHHsites_m) > 0:
# 			JiCHH_ms = []
# 			for CHHsite in CHHsites_m:
# 				sumofcread_CHH_m = creaddata[i][CHHsite]
# 				sumoftread_CHH_m = treaddata[i][CHHsite]
# 				if (sumofcread_CHH_m + sumoftread_CHH_m) >= 4.0:
# 					JiCHH_ms.append((sumofcread_CHH_m) / (sumofcread_CHH_m + sumoftread_CHH_m))
# 				else:
# 					# JiCHH_ms.append(J_bCHH)
# 					JiCHH_ms.append(np.nan)
# 			# JiCHH_m += round((((len(JiCHH_ms)*np.mean(JiCHH_ms)) + J_bCHH)/(len(JiCHH_ms)+1)), 4)
# 			JiCHH_m += round(np.mean(JiCHH_ms), 4)
# 		else:
# 			# JiCHH_m += round(J_bCHH, 4)
# 			JiCHH_m += np.nan

# 		# into list of tuples
# 		JiCs.append(((JiCG_p, JiCG_m), (JiCHG_p, JiCHG_m), (JiCHH_p, JiCHH_m)))

# 		# P: Probability of C contexts
# 		PiCG_p = float(ctxdata[i].isin(['X']).sum()) / len(ctxdata.index)
# 		PiCG_m = float(ctxdata[i].isin(['x']).sum()) / len(ctxdata.index)
# 		# print (float(ctxdata[i].isin(['X']).sum()), float(ctxdata[i].isin(['x']).sum()))
# 		PiCHG_p = float(ctxdata[i].isin(['Y']).sum()) / len(ctxdata.index)
# 		PiCHG_m = float(ctxdata[i].isin(['y']).sum()) / len(ctxdata.index)

# 		PiCHH_p = float(ctxdata[i].isin(['Z']).sum()) / len(ctxdata.index)
# 		PiCHH_m = float(ctxdata[i].isin(['z']).sum()) / len(ctxdata.index)

# 		# into list of tuples
# 		PiCs.append(((PiCG_p, PiCG_m), (PiCHG_p, PiCHG_m), (PiCHH_p, PiCHH_m)))
# 		# Need to normalize per site
# 		# CGsites = ctxdata.index[ctxdata[i].isin(['X'])].tolist()
# 		# CHGsites = ctxdata.index[ctxdata[i].isin(['Y'])].tolist()
# 		# CHHsites = ctxdata.index[ctxdata[i].isin(['Z'])].tolist()

# 		# CGmethyl = calMethyl(creaddata, treaddata, CGsites, i)
# 		# CHGmethyl = calMethyl(creaddata, treaddata, CHGsites, i)
# 		# CHHmethyl = calMethyl(creaddata, treaddata, CHHsites, i)

# 		Cmethyls.append((JiCG_p, JiCHG_p, JiCHH_p))
# 		Gmethyls.append((JiCG_m, JiCHG_m, JiCHH_m))

# 	Methyls = [list(sum(i, ())) for i in JiCs]
# 	Methyls = pd.DataFrame(Methyls, columns = ['CpG_p', 'CpG_m', 'CHG_p', 'CHG_m', 'CHH_p', 'CHH_m'])
# 	# Methyls.insert(loc = 0, column = 'position', value = range(1, motiflen + 1))	

# 	print ("Methylation probabilities: ")
# 	# print (Methyls)
# 	print ('\n')
# 	print (Methyls[100:113])

# 	return JiCs, PiCs, Cmethyls, Gmethyls, Methyls


#-----------------------------------------------------------

def methylationEntropy(JiCs, PiCs, J_bCG, J_bCHG, J_bCHH, logotype):
	
	""" Calculate relative entropy from methylation """

	Cents = []
	if logotype == 'Kullback-Liebler':
		for i in list(range(len(JiCs))):
			# print i
			JiCG_p, JiCHG_p, JiCHH_p = [j[0] for j in JiCs[i]]
			JiCG_m, JiCHG_m, JiCHH_m = [j[1] for j in JiCs[i]]
			PiCG_p, PiCHG_p, PiCHH_p = [j[0] for j in PiCs[i]]
			PiCG_m, PiCHG_m, PiCHH_m = [j[1] for j in PiCs[i]]
			CGent_p = 0.0
			if np.isnan(JiCG_p) or JiCG_p == 0.0:
				CGent_p += 0.0				
			else:
				CGent_p += PiCG_p * ((JiCG_p * math.log(JiCG_p/J_bCG, 2)) + (1 - JiCG_p) * math.log((1 - JiCG_p)/(1 - J_bCG), 2))

			CGent_m = 0.0
			if np.isnan(JiCG_m) or JiCG_m == 0.0:
				CGent_m += 0.0
			else:
				CGent_m += PiCG_m * ((JiCG_m * math.log(JiCG_m/J_bCG, 2)) + (1 - JiCG_m) * math.log((1 - JiCG_m)/(1 - J_bCG), 2))

			CHGent_p = 0.0
			if np.isnan(JiCHG_p) or JiCHG_p == 0.0:
				CHGent_p += 0.0
			else:
				CHGent_p += PiCHG_p * ((JiCHG_p * math.log(JiCHG_p/J_bCHG, 2)) + (1 - JiCHG_p) * math.log((1 - JiCHG_p)/(1 - J_bCHG), 2))
			
			CHGent_m = 0.0
			if np.isnan(JiCHG_m) or JiCHG_m == 0.0:
				CHGent_m += 0.0
			else:
				CHGent_m += PiCHG_m * ((JiCHG_m * math.log(JiCHG_m/J_bCHG, 2)) + (1 - JiCHG_m) * math.log((1 - JiCHG_m)/(1 - J_bCHG), 2))
			
			CHHent_p = 0.0
			if np.isnan(JiCHH_p) or JiCHH_p == 0.0:
				CHHent_p += 0.0
			else:
				CHHent_p += PiCHH_p * ((JiCHH_p * math.log(JiCHH_p/J_bCHH, 2)) + (1 - JiCHH_p) * math.log((1 - JiCHH_p)/(1 - J_bCHH), 2))
			
			CHHent_m = 0.0
			if np.isnan(JiCHH_m) or JiCHH_m == 0.0:
				CHHent_m += 0.0
			else:
				CHHent_m = PiCHH_m * ((JiCHH_m * math.log(JiCHH_m/J_bCHH, 2)) + (1 - JiCHH_m) * math.log((1 - JiCHH_m)/(1 - J_bCHH), 2))
			
			Cents.append((CGent_p, CGent_m, CHGent_p, CHGent_m, CHHent_p, CHHent_m))
	else:
		for i in range(len(JiCs)):
			JiCG_p, JiCHG_p, JiCHH_p = [j[0] for j in JiCs[i]]
			JiCG_m, JiCHG_m, JiCHH_m = [j[1] for j in JiCs[i]]
			PiCG_p, PiCHG_p, PiCHH_p = [j[0] for j in PiCs[i]]
			PiCG_m, PiCHG_m, PiCHH_m = [j[1] for j in PiCs[i]]

			CGent_p = 0.0
			if np.isnan(JiCG_p):
				CGent_p += 0.0
			else:
				CGent_p += PiCG_p * (1 - ((JiCG_p * math.log(JiCG_p, 2)) + (1 - JiCG_p) * math.log((1 - JiCG_p), 2)))
			
			CGent_m = 0.0
			if np.isnan(JiCG_m):
				CGent_m += 0.0
			else:
				CGent_m += PiCG_m * (1 - ((JiCG_m * math.log(JiCG_m, 2)) + (1 - JiCG_m) * math.log((1 - JiCG_m), 2)))
			
			CHGent_p = 0.0
			if np.isnan(JiCHG_p):
				CHGent_p += 0.0
			else:
				CHGent_p += PiCHG_p * (1 - ((JiCHG_p * math.log(JiCHG_p, 2)) + (1 - JiCHG_p) * math.log((1 - JiCHG_p), 2)))
			
			CHGent_m = 0.0
			if np.isnan(JiCHG_m):
				CHGent_m += 0.0
			else:
				CHGent_m += PiCHG_m * (1 - ((JiCHG_m * math.log(JiCHG_m, 2)) + (1 - JiCHG_m) * math.log((1 - JiCHG_m), 2)))			
			
			CHHent_p = 0.0
			if np.isnan(JiCHH_p):
				CHHent_p += 0.0
			else:
				CHHent_p += PiCHH_p * (1 - ((JiCHH_p * math.log(JiCHH_p, 2)) + (1 - JiCHH_p) * math.log((1 - JiCHH_p), 2)))
			
			CHHent_m = 0.0
			if np.isnan(JiCHH_m):
				CHHent_m += 0.0
			else:
				CHHent_m += PiCHH_m * (1 - ((JiCHH_m * math.log(JiCHH_m, 2)) + (1 - JiCHH_m) * math.log((1 - JiCHH_m), 2)))
			
			Cents.append((CGent_p, CGent_m, CHGent_p, CHGent_m, CHHent_p, CHHent_m))
	
	Cents = pd.DataFrame(Cents, columns = ['CpG_p', 'CpG_m', 'CHG_p', 'CHG_m', 'CHH_p', 'CHH_m'])
	Cents['Methylation'] = Cents[Cents.columns].sum(axis = 1)
	# print (Cents[flankinglength:flankinglength + motiflen + 1])

	return Cents

#-----------------------------------------------------------

def binning(Methyls):

	""" Smoothing methylation probability (level) """

	m_CpG = (Methyls['CpG_p'] + Methyls['CpG_m']) / 2.0
	m_CHG = (Methyls['CHG_p'] + Methyls['CHG_m']) / 2.0
	m_CHH = (Methyls['CHH_p'] + Methyls['CHH_m']) / 2.0

	scale = (flankinglength / binsize)
	print (scale)
	# Smooth Methylation level
	CpG_bin = []
	CHG_bin = []
	CHH_bin = []
	i = 0
	while i < scale:
		tmp1 = round(np.nanmean(m_CpG[binsize*i: binsize*i+binsize]), 4)
		CpG_bin.append(tmp1)
		tmp2 = round(np.nanmean(m_CHG[binsize*i: binsize*i+binsize]), 4)
		CHG_bin.append(tmp2)		
		tmp3 = round(np.nanmean(m_CHH[binsize*i: binsize*i+binsize]), 4)
		CHH_bin.append(tmp3)		
		i += 1

	CpG_bin.append(round(np.nanmean(m_CpG[flankinglength: flankinglength + motiflen]), 4))
	CHG_bin.append(round(np.nanmean(m_CHG[flankinglength: flankinglength + motiflen]), 4))
	CHH_bin.append(round(np.nanmean(m_CHH[flankinglength: flankinglength + motiflen]), 4))

	i = 0
	while i < scale:
		# print flankinglength + motiflen + binsize*i
		tmp1 = round(np.nanmean(m_CpG[flankinglength + motiflen + binsize*i: flankinglength + motiflen + binsize*i+binsize]), 4)
		CpG_bin.append(tmp1)
		tmp2 = round(np.nanmean(m_CHG[flankinglength + motiflen + binsize*i: flankinglength + motiflen + binsize*i+binsize]), 4)
		CHG_bin.append(tmp2)		
		tmp3 = round(np.nanmean(m_CHH[flankinglength + motiflen + binsize*i: flankinglength + motiflen + binsize*i+binsize]), 4)
		CHH_bin.append(tmp3)		
		i += 1

	# print zip(CpG_bin, CHG_bin, CHH_bin)
	# print (CpG_bin[flankinglength:flankinglength + motiflen + 1])

	return CpG_bin, CHG_bin, CHH_bin



#-----------------------------------------------------------

def set_fig():

	""" Set figure """
	
	# Height = None
	# figureheight = None
	# if (mode == 'Methyl'):
	# 	Height = math.ceil(max(entropys['Total']))
	# 	if Height <= 3.0:
	# 		Height = 3.0
	# 	else:
	# 		Height = 5.0		
	# 	if Height == 3.0:
	# 		figureheight = Height + 1.0
	# 	elif Height == 5.0:
	# 		figureheight = Height + 3.0
	# else:
	# 	Height = math.ceil(max(entropys['Base']))
	# 	if Height <= 3.0:
	# 		Height = 3.0
	# 	else:
	# 		Height = Height
	# 	figureheight = Height
	fig = plt.figure(figsize = (24, 10))
	# fig.tight_layout()
	return fig

#-----------------------------------------------------------

class ScapePlot:
	def __init__(self, fig, tissue, Cents, CpG_bin, CHG_bin, CHH_bin, PiCs, J_bCG, J_bCHG, J_bCHH):
		self.fig = fig
		self.tissue = tissue
		# self.base_heights = four_base_heights
		self.Cents = Cents
		# self.Cmethyls = Cmethyls
		# self.Gmethyls = Gmethyls
		# self.PiCs = PiCs
		# self.bgpps = bgpps
		self.CpG_bin = CpG_bin
		self.CHG_bin = CHG_bin
		self.CHH_bin = CHH_bin
		self.PiCs = PiCs
		self.J_bCG = J_bCG
		self.J_bCHG = J_bCHG
		self.J_bCHH = J_bCHH

		
	def plotScape(self):

		# remove axis content	
		self.fig.clf()
		self.fig.tight_layout()
		gs = GridSpec(
						3, 
						3, 
						width_ratios=[1, 1, 1], 
						height_ratios = [2, 6, 6], 
						wspace = 0.1, 
						hspace = 0.2,
						)
		ax1 = plt.subplot(gs[0])
		ax2 = plt.subplot(gs[1], sharex = ax1, sharey = ax1)
		ax3 = plt.subplot(gs[2], sharex = ax1, sharey = ax1) 
		
		ax4 = plt.subplot(gs[3], sharex = ax1)
		ax5 = plt.subplot(gs[4], sharex = ax1, sharey = ax4)
		ax6 = plt.subplot(gs[5], sharex = ax1, sharey = ax4)
		
		ax7 = plt.subplot(gs[6], sharex = ax1)
		ax8 = plt.subplot(gs[7], sharex = ax1, sharey = ax7)
		ax9 = plt.subplot(gs[8], sharex = ax1, sharey = ax7)

		# reset fig size
		# Height = None
		# figureheight = None
		# if (mode == 'Methyl'):
		# 	Height = math.ceil(max(self.entropys['Total']))
		# 	if Height <= 3.0:
		# 		Height = 3.0
		# 	else:
		# 		Height = 5.0		
		# 	if Height == 3.0:
		# 		figureheight = Height + 1.0
		# 	elif Height == 5.0:
		# 		figureheight = Height + 3.0
		# else:
		# 	Height = math.ceil(max(self.entropys['Base']))
		# 	if Height <= 3.0:
		# 		Height = 3.0
		# 	else:
		# 		Height = Height
		# 	figureheight = Height


		ax1.set_xlim(-flankinglength/20, 2*flankinglength+motiflen*flankinglength/100+flankinglength/20)
		for ax in [ax1, ax2, ax3]:
			ax.set_ylim(-0.2, 2.2)
		for ax in [ax4, ax5, ax6, ax7, ax8, ax9]:
			ax.set_ylim(-0.05, 1.05)
		
		CpG_p = self.Cents['CpG_p']
		CpG_m = self.Cents['CpG_m']

		CHG_p = self.Cents['CHG_p']
		CHG_m = self.Cents['CHG_m']

		CHH_p = self.Cents['CHH_p']
		CHH_m = self.Cents['CHH_m']
		
		# print CpG_p[flankinglength:flankinglength+motiflen]

		ax1_x_axis = list(range(flankinglength))
		# print(ax1_x_axis)
		ax1_x_axis.extend([flankinglength + int(i * flankinglength/100) for i in list(range(motiflen))])
		# print(ax1_x_axis)
		ax1_x_axis.extend(list(range(flankinglength + int(motiflen*flankinglength/100), flankinglength*2 + int(motiflen*flankinglength/100))))

		ax4_x_axis = None
		if smooth == 'Y':
			ax4_x_axis = [i + binsize/2.0 for i in range(0, flankinglength, binsize)]
			ax4_x_axis.append(flankinglength + motiflen*flankinglength/100/2.0)
			ax4_x_axis.extend([flankinglength + motiflen*flankinglength/100/2.0 + i + binsize/2.0 for i in range(0, flankinglength, binsize)])
		else:
			# ax4_x_axis = range(2*flankinglength + motiflen)
			ax4_x_axis = ax1_x_axis
		# 	ax4_x_axis = range(flankinglength)
		# 	ax4_x_axis.extend([flankinglength + i * flankinglength/100 for i in range(motiflen)])
		# 	ax4_x_axis.extend(range(flankinglength + motiflen*flankinglength/100, flankinglength*2 + motiflen*flankinglength/100))
		# print ax4_x_axis
		# print (len(ax4_x_axis))

		for ax in [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9]:
			ax.axvline(x = flankinglength, linewidth = 2, linestyle = '--', color = 'gray', alpha = 0.3)
			ax.axvline(x = flankinglength+motiflen*flankinglength/100, linewidth = 2, linestyle = '--', color = 'gray', alpha = 0.3)

		ax1.bar(ax1_x_axis, CpG_p.tolist(), width = 1, color = 'blue', label = 'CG')
		ax1.bar(ax1_x_axis, CpG_m.tolist(), bottom = CpG_p.tolist(), width = 1, color = 'blue', alpha = 0.5)
		
		ax2.bar(ax1_x_axis, CHG_p.tolist(), width = 1, color = 'lightgreen', label = 'CHG')
		ax2.bar(ax1_x_axis, CHG_m.tolist(), bottom = CHG_p.tolist(), width = 1, color = 'lightgreen', alpha = 0.5)
		
		ax3.bar(ax1_x_axis, CHH_p.tolist(), width = 1, color = 'red', label = 'CHH')
		ax3.bar(ax1_x_axis, CHH_m.tolist(), bottom = CHH_p.tolist(), width = 1, color = 'red', alpha = 0.5)
		
		# print self.CpG_bin[flankinglength:flankinglength+motiflen]
		# print self.CHG_bin[flankinglength:flankinglength+motiflen]
		# print self.CHH_bin[flankinglength:flankinglength+motiflen]

		ax4.plot(ax4_x_axis, self.CpG_bin, color = 'blue', label = 'CG')
		ax5.plot(ax4_x_axis, self.CHG_bin, color = 'lightgreen', label = 'CHG')
		ax6.plot(ax4_x_axis, self.CHH_bin, color = 'red', label = 'CHH')
		# ax4.plot(ax4_x_axis, [0.82]*len(ax4_x_axis), color = 'blue', linestyle = '--', label = 'CG background')
		ax4.plot(ax4_x_axis, [self.J_bCG]*len(ax4_x_axis), color = 'blue', alpha = 0.0)
		ax4.fill_between(ax4_x_axis, 0, [self.J_bCG]*len(ax4_x_axis), facecolor='blue', alpha=0.1)

		ax5.bar(ax4_x_axis, [self.J_bCHG]*len(ax4_x_axis), width = 1, color = 'lightgreen', alpha = 0.0)
		ax5.fill_between(ax4_x_axis, 0, [self.J_bCHG]*len(ax4_x_axis), facecolor='lightgreen', alpha=0.1)
		
		ax6.bar(ax4_x_axis, [self.J_bCHH]*len(ax4_x_axis), width = 1, color = 'red', alpha = 0.0)
		ax6.fill_between(ax4_x_axis, 0, [self.J_bCHH]*len(ax4_x_axis), facecolor='red', alpha=0.1)

		ax7.bar(ax1_x_axis, [i[0][0] for i in self.PiCs], width = 1, color = 'blue')
		ax7.bar(ax1_x_axis, [i[0][1] for i in self.PiCs], width = 1, color = 'blue', alpha = 0.5)

		ax8.bar(ax1_x_axis, [i[1][0] for i in self.PiCs], width = 1, color = 'lightgreen')
		ax8.bar(ax1_x_axis, [i[1][1] for i in self.PiCs], width = 1, color = 'lightgreen', alpha = 0.5)
		
		ax9.bar(ax1_x_axis, [i[2][0] for i in self.PiCs], width = 1, color = 'red')
		ax9.bar(ax1_x_axis, [i[2][1] for i in self.PiCs], width = 1, color = 'red', alpha = 0.5)

		# print [i[0][0] for i in self.PiCs][100:113]

		x_axis = list(range(0, flankinglength + 1, int(flankinglength/2)))
		# x_axis.extend(range(flankinglength + motiflen, 2 * flankinglength + motiflen + 1, flankinglength/2))
		x_axis.extend(range(flankinglength + int(motiflen*flankinglength/100), 2 * flankinglength + int(motiflen*flankinglength/100 + 1), int(flankinglength/2)))
		ax1.set_xticks(x_axis)

		# scale = flankinglength/50
		x_tick = [-flankinglength, -flankinglength/2]
		# ax1.set_xticks(x_tick)
		# x_tick = [-flankinglength + i*50 for i in range(scale)]
		x_tick.extend(['S', 'E'])
		x_tick.extend([flankinglength/2, flankinglength])		
		ax1.set_xticklabels(x_tick)		


		y_ticks = [round(0+i*0.2, 1) for i in range(6)]
		ax4.set_yticks(y_ticks)
		ax4.set_yticklabels(y_ticks)
		# print([0+i*0.2 for i in range(6)])

		# y_axis = [0 + 0.5*i for i in range(5)]	
		ax1.set_yticks([0.0, 1.0, 2.0])
		ax1.set_yticklabels([0.0, 1.0, 2.0])

		for ax in [ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9]:
			ax.spines['top'].set_linewidth(2)
			ax.spines['right'].set_linewidth(2)
			ax.spines['left'].set_linewidth(2)
			ax.spines['bottom'].set_linewidth(2)


		for ax in [ax1, ax4, ax7]:
			for label in ax.yaxis.get_ticklabels():
				label.set_fontweight('bold')
		for ax in [ax7, ax8, ax9]:
			for label in ax.xaxis.get_ticklabels():
				label.set_fontweight('bold')

		# seaborn.despine(ax = ax1, offset = 2, trim = True)

		for ax in [ax1, ax2, ax3, ax4, ax5, ax6]:
			ax.axes.get_xaxis().set_visible(False)
		for ax in [ax2, ax3, ax5, ax6, ax8, ax9]:
			ax.axes.get_yaxis().set_visible(False)


		for ax in [ax1, ax4, ax7]:
			ax.tick_params(
								direction = 'out', 
								length = 4, 
								width = 2, 
								labelsize = 14, #'x-large', 
								top = False, 
								right = False,
								bottom = False,
								)

		for ax in [ax7, ax8, ax9]:
			ax.tick_params(
							direction = 'out', 
							length = 4, 
							width = 2, 
							labelsize = 14, #'x-large', 
							top = False, 
							right = False,
							bottom = True,
							)
	
		
		ax1.set_ylabel(
						'Bits', 
						fontsize = 18, 
						weight = 'bold'
						)
		ax4.set_ylabel(
						'Probability',
						fontsize = 18,
						weight = 'bold'
						)
		ax7.set_ylabel(
						'Probability',
						fontsize = 18,
						weight = 'bold'
						)

		legend_properties = {'weight':'bold'}
		for ax in [ax1, ax2, ax3]:
			ax.legend(loc='upper right', fontsize = 'medium', prop=legend_properties)

		ax2.set_title('Relative entropy', fontsize = 18, weight = 'bold')
		ax5.set_title('Methylation probability', fontsize = 18, weight = 'bold')
		ax8.set_title('Context probability', fontsize = 18, weight = 'bold')

		plt.suptitle(TF, fontsize = 18, weight = 'bold')
		
		return ax1, ax2, ax3, ax4, ax5, ax6, ax7, ax8, ax9,

def main():

    # logging.basicConfig(filename="methylseqlogo.log", level = logging.DEBUG, format = '%(asctime)s, %(name)s-%(levelname)-s: %(message)s')
    pd.set_option('display.float_format', lambda x: '%.2f' % x)
    parser = get_parser()
    args = parser.parse_args()

    global dir_path
    dir_path = os.path.dirname(os.path.realpath(__file__))
    # print dir_path
    # seqfile = args.inputfasta
    global TF
    TF = args.transcriptionfactor  
    
    global species
    species = args.species
    
    global tissue
    tissue = args.tissuetypes
    
    global motiflen
    motiflen = int(args.motiflen)

    global flankinglength
    flankinglength = int(args.flankinglength)

    logotype = args.logotype
    
    global region
    region = args.regionofinterest

    global smooth
    smooth = args.smoothing

    # print dir_path
    # pd.set_option('display.precision', 4)
    
    species_tissue_pair = {
    						'human' : ['H1-hESC', 'HepG2', 'K562', 'GM12878', 'HeLa-S3', 'naive-hESC', 'primed-hESC'], 
    						'mouse' : ['mESC', 'mMPGC_E13p5', 'mMPGC_E16p5', 'mFPGC_E13p5', 'mFPGC_E16p5'], 
    						'arabidopsis' : ['Leaf', 'Inflorescence', 'Root'], 
    						'maize' : ['root', 'shoot']
    						}
    
    if tissue not in species_tissue_pair[species]:
    	print (tissue + " is not in" + " " + species + "'s tissue database.")
    	print (tissue + "不在" + species + "的組織資料庫內。")
    	print (tissue + "は" + species + "の組織データベースにはありませんでした。")
    	print ("Job cancelled." + "工作終止。" + "ジョブがキャンセルされました。")
    	raise SystemExit(0)

    seqfile = None
    ctxfile = None
    creaddata = None
    treaddata = None
    if region in ['whole_genome', 'promoter']:
    	seqfile = TF + '_' + tissue + '_' + region + '_binding_sites_neighbor_' + str(flankinglength) + '_seq.fa'
    	ctxfile = TF + '_' + tissue + '_' + region + '_binding_sites_neighbor_' + str(flankinglength) + '_ctx.txt'
    	creadfile = TF + '_' + tissue + '_' + region + '_binding_sites_neighbor_' + str(flankinglength) + '_cread.txt'
    	treadfile = TF + '_' + tissue + '_' + region + '_binding_sites_neighbor_' + str(flankinglength) + '_tread.txt'
    elif region in ['uniform']:
    	seqfile = TF + '_' + tissue + '_' + 'whole_genome' + '_binding_sites_neighbor' + str(flankinglength) + '_seq.fa'
    	ctxfile = TF + '_' + tissue + '_' + 'whole_genome' + '_binding_sites_neighbor' + str(flankinglength) + '_ctx.txt'
    	creadfile = TF + '_' + tissue + '_' + 'whole_genome' + '_binding_sites_neighbor' + str(flankinglength) + '_cread.txt'
    	treadfile = TF + '_' + tissue + '_' + 'whole_genome' + '_binding_sites_neighbor' + str(flankinglength) + '_tread.txt' 

    seqdata, ctxdata, creaddata, treaddata = datainput(seqfile, ctxfile, creadfile, treadfile)
    bgprobtable = species + '_' + tissue + '_probability.txt'
    # print (bgprobtable)
    bgpps, J_bCG, J_bCHG, J_bCHH = readProbTable(bgprobtable, region)
    JiCs, PiCs, Cmethyls, Gmethyls, Freqs_, Methyls  = MethylProbability(ctxdata, creaddata, treaddata, J_bCG, J_bCHG, J_bCHH)
    Cents = methylationEntropy(JiCs, PiCs, J_bCG, J_bCHG, J_bCHH, logotype)

    if smooth == 'Y':
	    global binsize
	    binsize = int(args.binsize)
	    bin_CpG, bin_CHG, bin_CHH = binning(Methyls)
	    fig = set_fig()
	    plotobj = ScapePlot(fig, tissue, Cents, bin_CpG, bin_CHG, bin_CHH, PiCs, J_bCG, J_bCHG, J_bCHH)
	    plotobj.plotScape()
	    figname = TF + '_' + species + '_' + tissue + '_' + region + '_neighbor_' + str(flankinglength) + '_bin_' + str(binsize) + '_MethylScape.png'
	    plt.savefig(dir_path + '/../Output/' + figname, bbox_inches = 'tight', dpi = 600)
    else:
	    fig = set_fig()
	    plotobj = ScapePlot(fig, tissue, Cents, (Methyls[['CpG_p', 'CpG_m']]).mean(axis = 1, skipna = True), (Methyls[['CHG_p', 'CHG_m']]).mean(axis = 1, skipna = True), (Methyls[['CHH_p', 'CHH_m']]).mean(axis = 1, skipna = True), PiCs, J_bCG, J_bCHG, J_bCHH)
	    plotobj.plotScape()
	    figname = TF + '_' + species + '_' + tissue + '_' + region + '_neighbor_' + str(flankinglength) + '_MethylScape.png'
	    plt.savefig(dir_path + '/../Output/' + figname, bbox_inches = 'tight', dpi = 600)

main()
