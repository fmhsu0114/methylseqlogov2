#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Author: feiman
# @Date:   2021-02-14 20:29:12
# @Last Modified by:   feiman
# @Last Modified time: 2022-07-10 14:01:31


""" python3 compatible MethylSeqLogo source code """

import os, sys, importlib
from Bio import SeqIO
import argparse, math
import pandas as pd
from decimal import *
import numpy as np
from operator import itemgetter
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.patheffects
from matplotlib.gridspec import GridSpec
from matplotlib.font_manager import FontProperties
import matplotlib.patches as patches
from matplotlib import transforms
from matplotlib import animation
import seaborn#, upsidedown





 #-----------------------------------------------------------

def get_parser():
    
	""" Create a parser and add arguments """
    
	parser= argparse.ArgumentParser()
	parser.add_argument('-T', '--transcriptionfactor', help= 'transcription factor', metavar= 'TF', required= True)
	parser.add_argument('-s', '--species', choices= ['human', 'mouse', 'arabidopsis', 'maize'], default= None, help= 'species of the input data', metavar= 'species', required= True)
	parser.add_argument('-t', '--tissuetypes', choices= ['H1-hESC', 'HepG2', 'K562', 'GM12878', 'HeLa-S3', 'naive-hESC', 'primed-hESC', 'mESC', 'mMPGC_E13p5', 'mMPGC_E16p5', 'mFPGC_E13p5', 'mFPGC_E16p5', 'Leaf', 'Inflorescence', 'Root', 'shoot'], default= None, help= 'type of tissue', metavar= 'tissue types')    
	parser.add_argument('-sp', '--spanwindowleft', help= 'start position of TFBS to plot logo', type= int, default= 1, required = False)
	parser.add_argument('-pl', '--plotlen', help= 'length of TFBS to plot logo', type= int, required = False)
	parser.add_argument('-m', '--mode', choices= ['Methyl', 'Regular'], default= 'Methyl', help= 'include methylation or not', metavar= 'mode of plotting')
	parser.add_argument('-l', '--logotype', choices= ['Shannon', 'Kullback-Liebler', 'riverlake', 'all'], default= 'Kullback-Liebler', help= 'logo type', metavar= 'type of logo')
	parser.add_argument('-r', '--regionofinterest', choices= ['promoter', 'whole_genome', 'uniform'], default= 'whole_genome', help= 'genomic region of interest', metavar= 'genomic region')
	parser.add_argument('-th', '--threshold', help= 'threshold to display methylation probability', default= 0.4, metavar= 'display threshold')

	return parser

#-----------------------------------------------------------

class Scale(matplotlib.patheffects.RendererBase):
    def __init__(self, sx, sy= None):
        self._sx= sx
        self._sy= sy

    def draw_path(self, renderer, gc, tpath, affine, rgbFace):
        affine= affine.identity().scale(self._sx, self._sy) + affine
        renderer.draw_path(gc, tpath, affine, rgbFace)

#-----------------------------------------------------------

def datainput(seqfasta, ctxfile, creadfile, treadfile):
	"""
	read TFBS fasta into pandas dataframe
	"""

	path= dir_path + "/../Input/"
	seqdata= SeqIO.to_dict(SeqIO.parse(path + seqfasta, 'fasta'))
	seqdata= pd.DataFrame.from_dict(seqdata, orient= 'index')
	seqdata.reset_index(drop= True)
	ctxdata= pd.read_csv(path + ctxfile, sep= "\t")
	creaddata= pd.read_csv(path + creadfile, sep= "\t")
	treaddata= pd.read_csv(path + treadfile, sep= "\t")

	return seqdata, ctxdata, creaddata, treaddata


#-----------------------------------------------------------

def seqToPFM(seqdata, pseudocount):

	pd.options.display.float_format = '{:,.4f}'.format
	pfm= []
	for i in seqdata:
		freqs= []
		freqs.append(float(seqdata[i].tolist().count('A')) + pseudocount)
		freqs.append(float(seqdata[i].tolist().count('C')) + pseudocount)
		freqs.append(float(seqdata[i].tolist().count('G')) + pseudocount)
		freqs.append(float(seqdata[i].tolist().count('T')) + pseudocount)
		pfm.append(freqs)

	ppm= []
	for freqs in pfm:
		pps= []
		for freq in freqs:
			pps.append(freq/sum(freqs))
		ppm.append(pps)
	
	ppm= pd.DataFrame(ppm, columns= ['A', 'C', 'G', 'T'])

	path= dir_path + "/../Output/"
	ppmname= TF + '_' + species + '_' + tissue + '_' + region + '_' + 'ppm.txt'
	ppm.to_csv(path + ppmname, sep= '\t', index= False, header= True, float_format= '%.4f')

	print("Position probability matrix: ")
	print(ppm)
	print('\n')

	return pfm, ppm

#-----------------------------------------------------------

def calculateMlevel(sitelist, creaddata, treaddata, i, J_bC):

	JiC_p = 0.0
	if len(sitelist)> 0:
		JiC_ps= []
		for site in sitelist:
			sumofcread_C_p= creaddata[i][site]
			sumoftread_C_p= treaddata[i][site]
			if (sumofcread_C_p + sumoftread_C_p)>= 4.0:  # depth >= 4.0
				JiC_ps.append((sumofcread_C_p) / (sumofcread_C_p + sumoftread_C_p))
			else:
				JiC_ps.append(J_bC)
		JiC_p += round((((len(JiC_ps)*np.mean(JiC_ps)) + J_bC)/(len(JiC_ps)+1)), 4)
	else:
		JiC_p += round(J_bC, 4)

	return JiC_p


#-----------------------------------------------------------

def MethylProbability(ctxdata, creaddata, treaddata, J_bCG, J_bCHG, J_bCHH):
	"""
	Calculate conditional methylation probabilities: CG, CHG, CHH
	"""
	JiCs= []
	PiCs= []	# For conditional entropy
	Cmethyls= []
	Gmethyls= []
	PiCs_= []	# For label methylation level
	PiGs_= []	# For label methylation level

	for i in list(ctxdata.columns.values):
		# J: Conditional probability given C contexts
		# JiCG_p, JiCG_m, JiCHG_p, JiCHG_m, JiCHH_p, JiCHH_m = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
		
		# CG probability forward strand
		CGsites_p= ctxdata.index[ctxdata[i].isin(['X'])].tolist()
		JiCG_p= calculateMlevel(CGsites_p, creaddata, treaddata, i, J_bCG)
		# if len(CGsites_p)> 0:
		# 	JiCG_ps= []
		# 	for CGsite in CGsites_p:
		# 		sumofcread_CG_p= creaddata[i][CGsite]
		# 		sumoftread_CG_p= treaddata[i][CGsite]
		# 		if (sumofcread_CG_p + sumoftread_CG_p)>= 4.0:  # depth >= 4.0
		# 			JiCG_ps.append((sumofcread_CG_p) / (sumofcread_CG_p + sumoftread_CG_p))
		# 		else:
		# 			JiCG_ps.append(J_bCG)
		# 	JiCG_p += round((((len(JiCG_ps)*np.mean(JiCG_ps)) + J_bCG)/(len(JiCG_ps)+1)), 4)
		# else:
		# 	JiCG_p += round(J_bCG, 4)

		# CG probability reverse strand
		CGsites_m= ctxdata.index[ctxdata[i].isin(['x'])].tolist()
		JiCG_m= calculateMlevel(CGsites_m, creaddata, treaddata, i, J_bCG)

		# if len(CGsites_m)> 0:
		# 	JiCG_ms= []
		# 	for CGsite in CGsites_m:
		# 		sumofcread_CG_m= creaddata[i][CGsite]
		# 		sumoftread_CG_m= treaddata[i][CGsite]
		# 		if (sumofcread_CG_m + sumoftread_CG_m)>= 4.0:
		# 			JiCG_ms.append((sumofcread_CG_m) / (sumofcread_CG_m + sumoftread_CG_m))
		# 		else:
		# 			JiCG_ms.append(J_bCG)
		# 	JiCG_m += round((((len(JiCG_ms)*np.mean(JiCG_ms)) + J_bCG)/(len(JiCG_ms)+1)), 4)
		# else:
		# 	JiCG_m += round(J_bCG, 4)

		# CHG probability forward strand
		CHGsites_p= ctxdata.index[ctxdata[i].isin(['Y'])].tolist()
		JiCHG_p= calculateMlevel(CHGsites_p, creaddata, treaddata, i, J_bCHG)
		# if len(CHGsites_p)> 0:
		# 	JiCHG_ps= []
		# 	for CHGsite in CHGsites_p:
		# 		sumofcread_CHG_p= creaddata[i][CHGsite]
		# 		sumoftread_CHG_p= treaddata[i][CHGsite]
		# 		if (sumofcread_CHG_p + sumoftread_CHG_p)>= 4.0:
		# 			JiCHG_ps.append((sumofcread_CHG_p) / (sumofcread_CHG_p + sumoftread_CHG_p))
		# 		else:
		# 			JiCHG_ps.append(J_bCHG)
		# 	JiCHG_p += round((((len(JiCHG_ps)*np.mean(JiCHG_ps)) + J_bCHG)/(len(JiCHG_ps)+1)), 4)
		# else:
		# 	JiCHG_p += round(J_bCHG, 4)

		# CHG probability forward strand
		CHGsites_m = ctxdata.index[ctxdata[i].isin(['y'])].tolist()
		JiCHG_m= calculateMlevel(CHGsites_m, creaddata, treaddata, i, J_bCHG)

		# if len(CHGsites_m) > 0:
		# 	JiCHG_ms = []
		# 	for CHGsite in CHGsites_m:
		# 		sumofcread_CHG_m = creaddata[i][CHGsite]
		# 		sumoftread_CHG_m = treaddata[i][CHGsite]
		# 		if (sumofcread_CHG_m + sumoftread_CHG_m) >= 4.0:
		# 			JiCHG_ms.append((sumofcread_CHG_m) / (sumofcread_CHG_m + sumoftread_CHG_m))
		# 		else:
		# 			JiCHG_ms.append(J_bCHG)
		# 	JiCHG_m += round((((len(JiCHG_ms)*np.mean(JiCHG_ms)) + J_bCHG)/(len(JiCHG_ms)+1)), 4)
		# else:
		# 	JiCHG_m += round(J_bCHG, 4)

		# CHH probability forward strand
		CHHsites_p = ctxdata.index[ctxdata[i].isin(['Z'])].tolist()
		JiCHH_p= calculateMlevel(CHHsites_p, creaddata, treaddata, i, J_bCHH)

		# if len(CHHsites_p) > 0:
		# 	JiCHH_ps = []
		# 	for CHHsite in CHHsites_p:
		# 		# JiCHH_ps = []
		# 		sumofcread_CHH_p = creaddata[i][CHHsite]
		# 		sumoftread_CHH_p = treaddata[i][CHHsite]
		# 	# JiCG_p = (sumofcread_CG_p + pseudocount*J_bCG) / (sumofcread_CG_p + sumoftread_CG_p + pseudocount)
		# 		if (sumofcread_CHH_p + sumoftread_CHH_p) >= 4.0:
		# 			JiCHH_ps.append((sumofcread_CHH_p) / (sumofcread_CHH_p + sumoftread_CHH_p))
		# 		else:
		# 			JiCHH_ps.append(J_bCHH)
		# 	JiCHH_p += round((((len(JiCHH_ps)*np.mean(JiCHH_ps)) + J_bCHH)/(len(JiCHH_ps)+1)), 4)
		# else:
		# 	JiCHH_p += round(J_bCHH, 4)

		# CHH probability reverse strand
		CHHsites_m = ctxdata.index[ctxdata[i].isin(['z'])].tolist()
		JiCHH_m= calculateMlevel(CHHsites_m, creaddata, treaddata, i, J_bCHH)

		# if len(CHHsites_m) > 0:
		# 	JiCHH_ms = []
		# 	for CHHsite in CHHsites_m:
		# 		sumofcread_CHH_m = creaddata[i][CHHsite]
		# 		sumoftread_CHH_m = treaddata[i][CHHsite]
		# 		if (sumofcread_CHH_m + sumoftread_CHH_m) >= 4.0:
		# 			JiCHH_ms.append((sumofcread_CHH_m) / (sumofcread_CHH_m + sumoftread_CHH_m))
		# 		else:
		# 			JiCHH_ms.append(J_bCHH)
		# 	JiCHH_m += round((((len(JiCHH_ms)*np.mean(JiCHH_ms)) + J_bCHH)/(len(JiCHH_ms)+1)), 4)
		# else:
		# 	JiCHH_m += round(J_bCHH, 4)

		# into list of tuples
		JiCs.append(((JiCG_p, JiCG_m), (JiCHG_p, JiCHG_m), (JiCHH_p, JiCHH_m)))

		# P: Probability of C contexts, denominator is the number of total sites
		PiCG_p= float(ctxdata[i].isin(['X']).sum()) / len(ctxdata.index)
		PiCG_m= float(ctxdata[i].isin(['x']).sum()) / len(ctxdata.index)
		# print (float(ctxdata[i].isin(['X']).sum()), float(ctxdata[i].isin(['x']).sum()))
		PiCHG_p= float(ctxdata[i].isin(['Y']).sum()) / len(ctxdata.index)
		PiCHG_m= float(ctxdata[i].isin(['y']).sum()) / len(ctxdata.index)

		PiCHH_p= float(ctxdata[i].isin(['Z']).sum()) / len(ctxdata.index)
		PiCHH_m= float(ctxdata[i].isin(['z']).sum()) / len(ctxdata.index)
		# into list of tuples
		PiCs.append(((PiCG_p, PiCG_m), (PiCHG_p, PiCHG_m), (PiCHH_p, PiCHH_m)))
		Cmethyls.append((JiCG_p, JiCHG_p, JiCHH_p))
		Gmethyls.append((JiCG_m, JiCHG_m, JiCHH_m))

		TotalC= float(ctxdata[i].isin(['X', 'Y', 'Z']).sum())
		if TotalC!= 0.0:
			PiCG_p_= float(ctxdata[i].isin(['X']).sum()) / TotalC
			PiCHG_p_= float(ctxdata[i].isin(['Y']).sum()) / TotalC
			PiCHH_p_= float(ctxdata[i].isin(['Z']).sum()) / TotalC
			PiCs_.append((PiCG_p_, PiCHG_p_, PiCHH_p_))
		else:
			PiCs_.append((0.0, 0.0, 0.0))

		TotalG= float(ctxdata[i].isin(['x', 'y', 'z']).sum())
		if TotalG!= 0.0:
			PiCG_m_= float(ctxdata[i].isin(['x']).sum()) / TotalG
			PiCHG_m_= float(ctxdata[i].isin(['y']).sum()) / TotalG
			PiCHH_m_= float(ctxdata[i].isin(['z']).sum()) / TotalG
			PiGs_.append((PiCG_m_, PiCHG_m_, PiCHH_m_))
		else:
			PiGs_.append((0.0, 0.0, 0.0))


	Methyls= [list(sum(i, ())) for i in JiCs]
	Methyls= pd.DataFrame(Methyls, columns= ['CpG_p', 'CpG_m', 'CHG_p', 'CHG_m', 'CHH_p', 'CHH_m'])

	Methyls.insert(loc= 0, column= 'position', value= list(range(1, plotlen + 1)))	

	print ("Methylation probabilities: ")
	print (Methyls)
	print ('\n')

	Freqs= [list(sum(i, ())) for i in PiCs]
	Freqs= pd.DataFrame(Freqs, columns= ['CpG_p', 'CpG_m', 'CHG_p', 'CHG_m', 'CHH_p', 'CHH_m'])
	Freqs.insert(loc= 0, column= 'position', value= list(range(1, plotlen + 1)))

	print ("Context probabilities: ")
	print (Freqs)
	print ('\n')

	# Freqs_ = [list(sum(i, ())) for i in PiCs]
	Freqs_C= pd.DataFrame(PiCs_, columns= ['CpG_p', 'CHG_p', 'CHH_p'])
	Freqs_C.insert(loc= 0, column= 'position', value= list(range(1, plotlen + 1)))
	Freqs_G = pd.DataFrame(PiGs_, columns= ['CpG_m', 'CHG_m', 'CHH_m'])
	Freqs_ = pd.concat([Freqs_C, Freqs_G], axis= 1)

	print ("Context probabilities (C only): ")
	print (Freqs_)
	print ('\n')

	return JiCs, PiCs, Cmethyls, Gmethyls, Freqs_

#-----------------------------------------------------------

def to4basedippm(seqdata):
	"""
	Build 4-letter dinucleotide probability matrix
	"""

	dinucs= []
	for i in nucleotides:
		for j in nucleotides:
			dinucs.append(i + j)

	probs= []
	for i in list(range(plotlen - 1)):
		prob= []
		row= zip(seqdata.iloc[:, i], seqdata.iloc[:, i+1])
		dinucseq= []
		for j, k in enumerate(row):
			string= ''.join(k)
			dinucseq.append(string)
		for dinuc in dinucs:
			pro= float(dinucseq.count(dinuc)+1)/ (len(seqdata.index)+16)
			prob.append(pro) # length = 16
		probs.append(prob)

	dippm= pd.DataFrame(probs, columns= dinucs)
	dippm= dippm.T.astype('float64')

	print ("Dinucleotide probability matrix: ")
	print (dippm)

	return dippm

#-----------------------------------------------------------

def readProbTable(infile, region):
	"""
	Read background probability table
	"""

	data= None
	path= dir_path + "/../Background_probability/"
	if os.path.isfile(path + infile):
		data = pd.read_table(path + infile, sep= "\t", header= 0, index_col= 0)
	else:
		print (infile + " does not exist. Job cancelled.")
		print (infile + "檔案不存在，工作終止。")
		print (infile + "はありませんでした。ジョブがキャンセルされました。")
		raise SystemExit(0)
		
	data= pd.DataFrame(data)

	probmatrix= data[region].astype('float64')
	# print (probmatrix)
	# probmatrix.to_numerics()

	bgpps= probmatrix[['A', 'C', 'G', 'T', 'CpG', 'CHG', 'CHH', 'AA', 'AC', 'AG', 'AT', 'CA', 'CC', 'CG', 'CT', 'GA', 'GC', 'GG', 'GT', 'TA', 'TC', 'TG', 'TT']]
	
	print ("background probabilities: ")
	print (bgpps)
	print ("\n")

	J_bCG= probmatrix['mCG'].astype('float64')
	J_bCHG= probmatrix['mCHG'].astype('float64')
	J_bCHH= probmatrix['mCHH'].astype('float64')

	print ("Background methylation probabilities: ")
	print (probmatrix[['mCG', 'mCHG', 'mCHH']])
	print ("\n")

	return bgpps, J_bCG, J_bCHG, J_bCHH

#-----------------------------------------------------------

def methylationEntropy(JiCs, PiCs, J_bCG, J_bCHG, J_bCHH, logotype):
	"""
	Calculate relative entropy from methylation
	"""

	Cents= []
	if logotype == 'Kullback-Liebler':
		for i in range(len(JiCs)):
			JiCG_p, JiCHG_p, JiCHH_p= [j[0] for j in JiCs[i]]
			JiCG_m, JiCHG_m, JiCHH_m= [j[1] for j in JiCs[i]]
			PiCG_p, PiCHG_p, PiCHH_p= [j[0] for j in PiCs[i]]
			PiCG_m, PiCHG_m, PiCHH_m= [j[1] for j in PiCs[i]]
			CGent_p= 0.0
			if np.isnan(JiCG_p):
				CGent_p+= 0.0				
			else:
				CGent_p+= PiCG_p * ((JiCG_p * math.log(JiCG_p/J_bCG, 2)) + (1 - JiCG_p) * math.log((1 - JiCG_p)/(1 - J_bCG), 2))

			CGent_m= 0.0
			if np.isnan(JiCG_m):
				CGent_m+= 0.0
			else:
				CGent_m+= PiCG_m * ((JiCG_m * math.log(JiCG_m/J_bCG, 2)) + (1 - JiCG_m) * math.log((1 - JiCG_m)/(1 - J_bCG), 2))

			CHGent_p= 0.0
			if np.isnan(JiCHG_p):
				CHGent_p+= 0.0
			else:
				CHGent_p+= PiCHG_p * ((JiCHG_p * math.log(JiCHG_p/J_bCHG, 2)) + (1 - JiCHG_p) * math.log((1 - JiCHG_p)/(1 - J_bCHG), 2))
			
			CHGent_m= 0.0
			if np.isnan(JiCHG_m):
				CHGent_m+= 0.0
			else:
				CHGent_m+= PiCHG_m * ((JiCHG_m * math.log(JiCHG_m/J_bCHG, 2)) + (1 - JiCHG_m) * math.log((1 - JiCHG_m)/(1 - J_bCHG), 2))
			
			CHHent_p= 0.0
			if np.isnan(JiCHH_p):
				CHHent_p+= 0.0
			else:
				CHHent_p+= PiCHH_p * ((JiCHH_p * math.log(JiCHH_p/J_bCHH, 2)) + (1 - JiCHH_p) * math.log((1 - JiCHH_p)/(1 - J_bCHH), 2))
			
			CHHent_m= 0.0
			if np.isnan(JiCHH_m):
				CHHent_m+= 0.0
			else:
				CHHent_m= PiCHH_m * ((JiCHH_m * math.log(JiCHH_m/J_bCHH, 2)) + (1 - JiCHH_m) * math.log((1 - JiCHH_m)/(1 - J_bCHH), 2))
			
			Cents.append((CGent_p, CGent_m, CHGent_p, CHGent_m, CHHent_p, CHHent_m))
	else:
		for i in range(len(JiCs)):
			JiCG_p, JiCHG_p, JiCHH_p = [j[0] for j in JiCs[i]]
			JiCG_m, JiCHG_m, JiCHH_m = [j[1] for j in JiCs[i]]
			PiCG_p, PiCHG_p, PiCHH_p = [j[0] for j in PiCs[i]]
			PiCG_m, PiCHG_m, PiCHH_m = [j[1] for j in PiCs[i]]

			CGent_p= 0.0
			if np.isnan(JiCG_p):
				CGent_p+= 0.0
			else:
				CGent_p+= PiCG_p * (1 - ((JiCG_p * math.log(JiCG_p, 2)) + (1 - JiCG_p) * math.log((1 - JiCG_p), 2)))
			
			CGent_m= 0.0
			if np.isnan(JiCG_m):
				CGent_m+= 0.0
			else:
				CGent_m+= PiCG_m * (1 - ((JiCG_m * math.log(JiCG_m, 2)) + (1 - JiCG_m) * math.log((1 - JiCG_m), 2)))
			
			CHGent_p= 0.0
			if np.isnan(JiCHG_p):
				CHGent_p+= 0.0
			else:
				CHGent_p+= PiCHG_p * (1 - ((JiCHG_p * math.log(JiCHG_p, 2)) + (1 - JiCHG_p) * math.log((1 - JiCHG_p), 2)))
			
			CHGent_m= 0.0
			if np.isnan(JiCHG_m):
				CHGent_m+= 0.0
			else:
				CHGent_m+= PiCHG_m * (1 - ((JiCHG_m * math.log(JiCHG_m, 2)) + (1 - JiCHG_m) * math.log((1 - JiCHG_m), 2)))			
			
			CHHent_p= 0.0
			if np.isnan(JiCHH_p):
				CHHent_p+= 0.0
			else:
				CHHent_p+= PiCHH_p * (1 - ((JiCHH_p * math.log(JiCHH_p, 2)) + (1 - JiCHH_p) * math.log((1 - JiCHH_p), 2)))
			
			CHHent_m= 0.0
			if np.isnan(JiCHH_m):
				CHHent_m+= 0.0
			else:
				CHHent_m+= PiCHH_m * (1 - ((JiCHH_m * math.log(JiCHH_m, 2)) + (1 - JiCHH_m) * math.log((1 - JiCHH_m), 2)))
			
			Cents.append((CGent_p, CGent_m, CHGent_p, CHGent_m, CHHent_p, CHHent_m))
	
	Cents= pd.DataFrame(Cents, columns= ['CpG_p', 'CpG_m', 'CHG_p', 'CHG_m', 'CHH_p', 'CHH_m'])
	Cents['Methylation']= Cents[Cents.columns].sum(axis= 1)

	return Cents

#-----------------------------------------------------------

def totalEntropy(mfourletterppm, bgpps, Cents, logotype):
	"""
	Calculate column entropy
	"""

	# entropys from bases, mode including Shannon and KL
	pwmentropys= []
	if logotype == 'Kullback-Liebler':
		print ("Kullback-Liebler distance: ")
		for i in list(range(len(mfourletterppm))):
			pos_ent= 0
			pps= mfourletterppm.iloc[i]
			for (pp1, pp2) in zip(pps, bgpps):
				ent= (pp1 * math.log(pp1/pp2, 2))
				pos_ent+= ent
			pwmentropys.append(pos_ent)
	else:
		print ("Shannon entropy: ")
		for i in list(range(len(mfourletterppm))):
			pos_ent= 0
			pps= mfourletterppm.iloc[i]
			for pp1 in pps:
				ent= -(pp1 * math.log(pp1, 2))
				pos_ent+= ent
			pwmentropys.append(2 - pos_ent)

	# make table of entropys
	# Mentropys = [sum(i) for i in Cents]
	entropys = Cents.assign(Base= pd.Series(pwmentropys))
	# entropys = pd.DataFrame.from_records(zip(Mentropys, pwmentropys), columns = ['Methylation', 'Base'])
	entropys['Total']= entropys.loc[:, entropys.columns.isin(['Methylation', 'Base'])].sum(axis= 1)
	entropys.insert(loc= 0, column= 'position', value= range(1, plotlen + 1))
	print (entropys)
	print ("\n")

	path= dir_path + "/../Output/"
	if logotype == 'Kullback-Liebler':
		icname= TF + '_' + species + '_' + tissue + '_' + region + '_' + 'relative_entropy.txt'
		entropys.to_csv(path + icname, sep= '\t', index= False, header= True, float_format= '%.2f')

	else:
		icname= TF + '_' + species + '_' + tissue + '_' + region + '_' + 'Shannon_entropy.txt'
		entropys.to_csv(path + icname, sep= '\t', index= False, header= True, float_format= '%.2f')

	return entropys

#-----------------------------------------------------------

def perBaseEntropy(entropys, mfourletterppm):
	"""
	Calculate per base nucleotide height (4-letter)
	"""

	four_base_heights= []
	
	for i in list(range(len(mfourletterppm))):
		base_ents= []
		for j in nucleotides:
			base_ent= None
			if mode == 'Methyl':
				base_ent = round(entropys['Base'][i] * mfourletterppm[j][i], 5)
				# base_ent = round(entropys['Total'][i] * mfourletterppm[j][i], 5)
				# base_ents.append((j, base_ent))
			else:
				base_ent= round(entropys['Base'][i] * mfourletterppm[j][i], 5)
			base_ents.append((j, base_ent))
		base_ents= sorted(base_ents, key= itemgetter(1, 0))
		four_base_heights.append(base_ents)
	
	for i, four_base_height in enumerate(four_base_heights):
		four_base_height= sorted(four_base_height, key= itemgetter(1, 0))

	path= dir_path + "/../Output/"
	filename= TF + '_' + species + '_' + tissue + '_' + region + '_per_base_information_content.txt'
	try:
		with open(path + filename, "w") as outfile:
			outfile.write("position" + "\t" + "A" + "\t" + "C" + "\t" + "G" + "\t" + "T" + "\n")
			for i, four_base_height in enumerate(four_base_heights):
				four_base_height= sorted(four_base_height, key= itemgetter(0))
				outfile.write(str(i + 1) + "\t" + str("%.2f" % four_base_height[0][1]) + "\t" + str("%.2f" % four_base_height[1][1]) + "\t" + str("%.2f" % four_base_height[2][1])+ "\t" + str("%.2f" % four_base_height[3][1]) + "\n")
	except IOError as e:
		print ("Unable to open file: " + filename)
		print (filename + "檔案無法開啟。")
		print (filename + "ファイルをオプン出来ませんでした。")

	# print (four_base_heights)

	return four_base_heights

#-----------------------------------------------------------

def twomerBg(bgpps, dippm):

	dinucs= []
	for i in nucleotides:
		for j in nucleotides:
			dinucs.append(i + j)

	dimerenrichments= {}
	for i in nucleotides:		
		for j in nucleotides:
			dinuc= i+j
			entscore= math.log(bgpps.loc[dinuc], 2) - math.log(bgpps.loc[i]*bgpps.loc[j], 2)
			dimerenrichments[dinuc]= entscore

	dientropis= {}
	for dinuc in dinucs:
		dientropis[dinuc]= []
	for i in list(range(plotlen - 1)):
		for dinuc in dinucs:
			d1= dinuc[0]
			d2= dinuc[1]
			dientropis[dinuc].append(dippm.loc[dinuc][i] * (math.log(bgpps.loc[d1]*bgpps.loc[d2], 2) - math.log(bgpps.loc[dinuc], 2)))
	print ("\n")
	print ("Dimer enrichment scores: " + "\n")
	series= pd.Series(dimerenrichments)
	print (series)
	print ("\n")

	print ("Depleted dimer binding scores: " + "\n")
	series2= pd.DataFrame.from_dict(dientropis, orient = 'columns')
	series2['Total']= series2.sum(axis = 1)
	series2.insert(loc= 0, column= 'position', value= [str(i) + '-' + str(i+1) for i in list(range(1, plotlen))])
	print (series2)
	print ("\n")

	bg_dientropis= {}
	for dinuc in dinucs:
		d1= dinuc[0]
		d2= dinuc[1]
		bg_dientropis[dinuc]= ((math.log(bgpps.loc[d1]*bgpps.loc[d2], 2) - math.log(bgpps.loc[dinuc], 2)))

	# print (bg_dientropis)

	return series2['CG'].tolist(), bg_dientropis['CG'], bg_dientropis['CC']

#-----------------------------------------------------------

def set_fig(entropys):
	"""
	Set figure size
	"""
	
	Height= None
	figureheight= None
	if logotype == 'riverlake':
		Height= 3.0
		figureheight= 3.0
	else:
		if (mode == 'Methyl'):
			# Height = math.ceil(max(entropys['Base']))
			Height= 3.0
			figureheight= Height + 4.0
		else:
			Height= math.ceil(max(entropys['Base']))
			if Height<= 3.0:
				Height= 3.0
			else:
				Height= Height
			figureheight= Height
	fig= plt.figure(figsize = (plotlen + 1, figureheight))
	# fig.tight_layout()
	return fig

#-----------------------------------------------------------

class seqLogoPlot:
	def __init__(self, fig, tissue, four_base_heights, entropys, Cmethyls, Gmethyls, bgpps, dientropys, bg_dientropys_max, bg_dientropys_min, J_bCG, J_bCHG, J_bCHH, Freqs_):
		self.fig = fig
		self.tissue = tissue
		self.base_heights = four_base_heights
		self.entropys = entropys
		self.Cmethyls = Cmethyls
		self.Gmethyls = Gmethyls
		self.bgpps = bgpps
		self.dientropys = dientropys
		self.dientropys = dientropys
		self.bg_dientropys_max = bg_dientropys_max
		self.bg_dientropys_min = bg_dientropys_min
		self.J_bCG = J_bCG
		self.J_bCHG = J_bCHG
		self.J_bCHH = J_bCHH
		self.Freqs_ = Freqs_
		
	def plotlogo(self):
		
		# remove axis content	
		self.fig.clf()
		self.fig.tight_layout()

		# reset fig size
		Height = None
		figureheight = None
		if (mode == 'Methyl'):
			Height = 3.0
			figureheight = Height + 4.0
			# Height = math.ceil(max(self.entropys['Total']))
			# if Height <= 3.0:
			# 	Height = 3.0
			# else:
			# 	Height = 5.0		
			# if Height == 3.0:
			# 	figureheight = Height + 4.0
			# elif Height == 5.0:
			# 	figureheight = Height + 6.0 
		else:
			Height = math.ceil(max(self.entropys['Base']))
			if Height <= 3.0:
				Height = 3.0
			else:
				Height = Height
			figureheight = Height
		
		# set subplots if it is 'Methyl' mode
		if (mode == 'Methyl'):
			gs = GridSpec(
							3, #row
							2, #column
							width_ratios=[plotlen + 1, 1], 
							height_ratios = [3, 6, 3], 
							wspace = 0.0, 
							hspace = 0.5,
							)

			ax1 = plt.subplot(gs[2])
			ax2 = plt.subplot(gs[3], sharey = ax1)
			ax3 = plt.subplot(gs[0], sharex = ax1) 
			ax4 = plt.subplot(gs[1], sharex = ax2)#, sharey = ax3)
			ax5 = plt.subplot(gs[4], sharex = ax1)
			ax6 = plt.subplot(gs[5], sharey = ax5)


			x_axis = list(range(plotlen))
			ax1.set_xticks(x_axis)
			ax1.set_xticklabels([str(i+1) for i in x_axis])
			
			y_axis = range(int(Height) + 1)

			fontT = FontProperties()
			fontT.set_size(68)
			fontT.set_weight('bold')
			fontT.set_family('monospace')

			fontC = FontProperties()
			fontC.set_size(66)
			fontC.set_weight('bold')
			fontC.set_family('monospace')

			bbox_props = dict(boxstyle = "square, pad = 0.0", fill = 0, lw = 0.0, alpha = 0.5)
			
			base_heights = zip(self.base_heights, self.Cmethyls, self.Gmethyls) #[2:motiflen-2]
			
			for i, j in enumerate(base_heights):
				xshift = 0
				yshift = 0
				Bottom = 0
				width = 1

				for (base, score) in j[0]:
					
					ax1.bar(
							x_axis[i], 
							score,
							bottom = Bottom,
							width = width,
							color = COLOR_SCHEME[base],
							alpha = 0.0
							)

					if base in ['C', 'G']:
						yshift = 1.0
						txt1 = ax1.text(
										x_axis[i],
										Bottom,
										base,
										fontproperties = fontC,
										transform = transforms.offset_copy(
						           											ax1.transData, 
						           											fig = self.fig,
						           											x = -6,
						           											y = yshift*(score), 
						           											units = 'points'
						           										),
										ha = 'center',
										va = 'baseline',
										color = COLOR_SCHEME[base],
										bbox = bbox_props,
										zorder = 1,
										)
						txt2 = ax1.text(
										x_axis[i],
										Bottom,
										base,
										fontproperties = fontC,
										transform = transforms.offset_copy(
						           											ax1.transData, 
						           											fig = self.fig,
						           											x = -6,
						           											y = yshift*(score), 
						           											units = 'points'
						           										),
										clip_on = True,
										ha = 'center',
										va = 'baseline',
										color = 'black',
										bbox = bbox_props,
										zorder = 2,
										)
						for txt in [txt1, txt2]:
							txt.set_path_effects([Scale(1.2, score)])

						# Shading methylation
						
						bgMlevel = None
						foreMlevel = None
						if base == 'C':
							bgMlevel = score * (self.Freqs_.iloc[i]['CpG_p'] * self.J_bCG + self.Freqs_.iloc[i]['CHG_p'] * self.J_bCHG + self.Freqs_.iloc[i]['CHH_p'] * self.J_bCHH)
							foreMlevel = score * (self.Freqs_.iloc[i]['CpG_p'] * j[1][0] + self.Freqs_.iloc[i]['CHG_p'] * j[1][1] + self.Freqs_.iloc[i]['CHH_p'] * j[1][2])
						elif base == 'G':
							bgMlevel = score * (self.Freqs_.iloc[i]['CpG_m'] * self.J_bCG + self.Freqs_.iloc[i]['CHG_m'] * self.J_bCHG + self.Freqs_.iloc[i]['CHH_m'] * self.J_bCHH)
							foreMlevel = score * (self.Freqs_.iloc[i]['CpG_m'] * j[2][0] + self.Freqs_.iloc[i]['CHG_m'] * j[2][1] + self.Freqs_.iloc[i]['CHH_m'] * j[2][2])

						# shading, first patch methylated part and then clip unmethylated							
						bot = Bottom
						left = x_axis[i] - 0.5	

						p1 = patches.Rectangle(
												(left, bot),
												1.0,
												foreMlevel,
												clip_on = True,
												lw = 0,
												fill = False
												)

						ax1.add_patch(p1)
						txt2.set_clip_path(p1)
						# print threshold
						if score >= threshold: # thresholding
							ax1.plot(
										(left, left+1), 
										(bot + bgMlevel, bot + bgMlevel),
										linewidth = 1.0,
										linestyle = '--',
										color = 'lightgray',
										zorder = 3,
									)

					else:
						txt1 = ax1.text(
										x_axis[i],
										Bottom,
										base,
										fontproperties = fontT,
										ha = 'center',
										va = 'baseline',
										color = COLOR_SCHEME[base],
										bbox = bbox_props
										)
						txt1.set_path_effects([Scale(1.0, score)])

					Bottom += score

			# add key to logo
			fontKey = FontProperties()
			fontKey.set_size(19)
			fontKey.set_weight('bold')
			fontKey.set_family('monospace')
			ax2top = int(Height)
			methylkeyori = ax2top - 1.0
			ax2.set_xlim(0.0, 1.2)
			ax2.set_ylim(0.0, int(Height))
			
			CGC = ax2.text(0.2, methylkeyori + 0.65, 'C', color = 'white', ha = 'center', fontproperties = fontKey,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			CGG = ax2.text(0.5, methylkeyori + 0.65, 'G', color = 'white', ha = 'center', fontproperties = fontKey,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			cgc = ax2.text(0.2, methylkeyori + 0.65, 'C', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			cgg = ax2.text(0.5, methylkeyori + 0.65, 'G', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)

			for txt in [CGC, CGG, cgc, cgg]:
				txt.set_path_effects([matplotlib.patheffects.Stroke(linewidth=2, foreground='black'), matplotlib.patheffects.Normal()])
				

			p1 = patches.Rectangle(
									(0.0, methylkeyori + 0.65), 
									1.0, 
									self.J_bCG*0.33, # height of the patch
									clip_on = True,
									lw = 0,
									fill = False
									)					
			ax2.add_patch(p1)
			cgc.set_clip_path(p1)
			cgg.set_clip_path(p1)

			CHGC = ax2.text(0.2, methylkeyori + 0.325, 'C', color = 'white', ha = 'center', fontproperties = fontKey, 
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			CHGH = ax2.text(0.5, methylkeyori + 0.325, 'H', color = 'white', ha = 'center', fontproperties = fontKey,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			CHGG = ax2.text(0.8, methylkeyori + 0.325, 'G', color = 'white', ha = 'center', fontproperties = fontKey,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)

			chgc = ax2.text(0.2, methylkeyori + 0.325, 'C', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			chgh = ax2.text(0.5, methylkeyori + 0.325, 'H', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			chgg = ax2.text(0.8, methylkeyori + 0.325, 'G', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)

			for txt in [CHGC, CHGH, CHGG, chgc, chgh, chgg]:
				txt.set_path_effects([matplotlib.patheffects.Stroke(linewidth=2, foreground='black'), matplotlib.patheffects.Normal()])

			p2 = patches.Rectangle(
									(0.0, methylkeyori + 0.325), 
									1.0, 
									self.J_bCHG*0.33, # height of the patch
									clip_on = True,
									lw = 0,
									fill = False
									)					
			ax2.add_patch(p2)
			chgc.set_clip_path(p2)
			chgh.set_clip_path(p2)
			chgg.set_clip_path(p2)

			CHHC = ax2.text(0.2, methylkeyori, 'C', color = 'white', ha = 'center', fontproperties = fontKey, 
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			CHHH1 = ax2.text(0.5, methylkeyori, 'H', color = 'white', ha = 'center', fontproperties = fontKey,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			CHHH2 = ax2.text(0.8, methylkeyori, 'H', color = 'white', ha = 'center', fontproperties = fontKey,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)

			chhc = ax2.text(0.2, methylkeyori, 'C', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			chhh1 = ax2.text(0.5, methylkeyori, 'H', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)
			chhh2 = ax2.text(0.8, methylkeyori, 'H', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
						   transform = transforms.offset_copy(
							           							ax2.transData, 
							           							fig = self.fig,
							           							x = 0.0,
							           							y = 1.5, 
							           							units = 'points'
							           										),)

			for txt in [CHHC, CHHH1, CHHH2, chhc, chhh1, chhh2]:
				# txt.set_path_effects([Scale(1.0, self.bgpps[nt])])
				txt.set_path_effects([matplotlib.patheffects.Stroke(linewidth=2, foreground='black'), matplotlib.patheffects.Normal()])

			p3 = patches.Rectangle(
									(0.0, methylkeyori), 
									1.0, 
									self.J_bCHH*0.33, # height of the patch
									clip_on = True,
									lw = 0,
									fill = False
									)					
			ax2.add_patch(p3)
			chhc.set_clip_path(p3)
			chhh1.set_clip_path(p3)
			chhh2.set_clip_path(p3)

			# frequency key
			freqkeyori = ax2top - 3

			bot = freqkeyori + 0.0
			for nt in sorted(['A', 'C', 'G', 'T'], reverse = True):
				freq = round(self.bgpps[nt], 4)
				bar = ax2.bar(0.5, freq, bottom = bot, width = 1.2, color = COLOR_SCHEME[nt], alpha = 0.0)
				fontKey2 = FontProperties()
				if nt in ['C', 'G']:
					fontKey2.set_size(60)
					fontKey2.set_weight('bold')
					fontKey2.set_family('monospace')
					txt1 = ax2.text(
									0.5, 
									bot, 
									nt, 
									ha = 'center', 
									va = 'baseline', 
									fontproperties = fontKey2, 
									transform = transforms.offset_copy(
						           											ax2.transData, 
						           											fig = self.fig,
						           											x = -5,
						           											y = 2.0*(self.bgpps[nt]), 
						           											units = 'points'
						           										),
									color = COLOR_SCHEME[nt],
									zorder = 1,
									)
					txt2 = ax2.text(
									0.5, 
									bot, 
									nt, 
									ha = 'center', 
									va = 'baseline', 
									fontproperties = fontKey2, 
									transform = transforms.offset_copy(
						           											ax2.transData, 
						           											fig = self.fig,
						           											x = -5,
						           											y = 2.0*(self.bgpps[nt]), 
						           											units = 'points'
						           										),
									color = 'black',
									clip_on = True,
									zorder = 2,
									)
					for txt in [txt1, txt2]:
						txt.set_path_effects([Scale(1.2, 2.2*self.bgpps[nt])])

					bgMlevel = self.bgpps['CpG'] * self.J_bCG + self.bgpps['CHG'] * self.J_bCHG + self.bgpps['CHH'] * self.J_bCHH
					pb = patches.Rectangle(
											(0.0, bot),
											1.0,
											bgMlevel * 2.2 * self.bgpps[nt], # shading methylation level
											clip_on = True,
											lw = 0,
											fill = False
											)

					ax2.add_patch(pb)
					txt2.set_clip_path(pb)

					ax2.plot(
								(0.0, 1.0), # x range
								(bot + bgMlevel * 2.2 * self.bgpps[nt], bot + bgMlevel * 2.2 * self.bgpps[nt]), # y range
								linestyle = '--',
								linewidth = 1,
								color = 'lightgrey',
								)
				else:
					fontKey2.set_size(62)
					fontKey2.set_weight('bold')
					fontKey2.set_family('monospace')
					txt = ax2.text(
									0.5, 
									bot, 
									nt, 
									ha = 'center', 
									va = 'baseline', 
									fontproperties = fontKey2, 
									color = COLOR_SCHEME[nt]
									)
					txt.set_path_effects([Scale(1.0, 2.2*self.bgpps[nt])])
				bot = bot + freq*2

			p3 = patches.Rectangle(
									(-0.1, freqkeyori-0.05), 
									1.2, 
									3.1, # height of the patch
									clip_on = False,
									lw = 1.5,
									fill = False
									)
			ax2.add_patch(p3)
			ax2.set_axis_off()
			ax2.text(	
						0.5, 
						-0.5, 
						'MethylSeqLogo', 
						ha = 'center', 
						va = 'bottom',
						fontsize = 10,
						weight = 'bold',
						)

			# plot methylation entropy on top
			ax3height = None
			if Height == 3.0:
				ax3height = 2
			else:
				ax3height = 5

			ax3.hlines(0, 0, plotlen-1, color = 'blue', linewidth = 0.5)

			ax3.set_ylim(-(ax3height/2), ax3height/2)			
			ax3_y_axis = [-(int(ax3height/2)), int(0), int(ax3height/2)]
			ax3.set_yticks(ax3_y_axis)
			ax3.set_yticklabels([abs(i) for i in ax3_y_axis])

			for i, cent in self.entropys.loc[:, self.entropys.columns.isin(['CpG_p', 'CpG_m', 'CHG_p', 'CHG_m', 'CHH_p', 'CHH_m'])].iterrows():
				Bottom_p = 0
				Bottom_m = 0
				cg_p, cg_m, chg_p, chg_m, chh_p, chh_m = cent
				
				ax3.bar(
						x_axis[i],
					    cg_p,
					    bottom = Bottom_p,
					    width = 0.8,
					    color = 'blue',
					    alpha = 0.8,
					    )
				Bottom_p += cg_p
				
				ax3.bar(
						x_axis[i],
					    -cg_m,
					    bottom = Bottom_m,
					    width = 0.8,
					    color = 'blue',
					    alpha = 0.4,
					    )
				Bottom_m -= cg_m
				
				ax3.bar(
						x_axis[i],
					    chg_p,
					    bottom = Bottom_p,
					    width = 0.8,
					    color = 'lime',
					    alpha = 0.8,
					    )
				Bottom_p += chg_p
				
				ax3.bar(
						x_axis[i],
					    -chg_m,
					    bottom = Bottom_m,
					    width = 0.8,
					    color = 'lime',
					    alpha = 0.4,
					    )
				Bottom_m -= chg_m
				
				ax3.bar(
						x_axis[i],
					    chh_p,
					    bottom = Bottom_p,
					    width = 0.8,
					    color = 'red',
					    alpha = 0.8,
					    )
				Bottom_p += chh_p
				ax3.bar(
						x_axis[i],
					    -chh_m,
					    bottom = Bottom_m,
					    width = 0.8,
					    color = 'red',
					    alpha = 0.4,
					    )
				Bottom_m += chh_m

			params = {'mathtext.default': 'regular' }  
			plt.rcParams.update(params)

			font = FontProperties()
			font.set_size(12)
			font.set_weight('bold')
			font.set_family('monospace')

			ax3.text(
						-0.5, 
						max(ax3_y_axis),
						r'+ strand',
						color = 'black',
						ha = 'left',
						va = 'center',
						fontproperties = font,
				)
			ax3.text(
						-0.5,
						min(ax3_y_axis), 
						# -ax3height/2 + 0.1,
						r'- strand',
						color = 'black',
						ha = 'left',
						va = 'center',
						fontproperties = font,
				)

			# ax3.text(	
			# 			0.5, 
			# 			2.0, 
			# 			'Methylation entropy', 
			# 			ha = 'left', 
			# 			va = 'center',
			# 			color = 'blue',
			# 			fontsize = 10,
			# 			weight = 'bold',
						# )
			# ax3.set_ylabel('Bits', fontsize = 18, weight = 'bold')


			ax3.spines['top'].set_linewidth(0)
			ax3.spines['right'].set_linewidth(0)
			ax3.spines['left'].set_linewidth(3)
			ax3.spines['bottom'].set_linewidth(0)
			seaborn.despine(ax = ax3, offset = 2, trim = True)
			
			for label in ax3.yaxis.get_ticklabels():
			    label.set_fontweight('bold')
			ax3.tick_params(
							direction = 'out', 
							length = 4, 
							width = 3, 
							labelsize = 18,#'x-large', 
							top = False, 
							right = False, 
							bottom = False)
			ax3.axes.get_xaxis().set_visible(False)
			# ax3.set_ylabel('Bits', fontsize = 18, weight = 'bold')

			ax4height = None
			if ax3height == 2:
				ax4height = 2
			else:
				ax4height = 2.5

			ax4.set_ylim(-(ax4height/2), ax4height/2)			
			ax4_y_axis = [-(ax4height/2), 0, ax4height/2]
			ax4.set_yticks(ax4_y_axis)
			ax4.set_yticklabels([int(ax4height/2), abs(0), abs(ax4height/2)])
			# print ax4_y_axis
			ax4ori = None
			if ax4height == 2:
				ax4ori = 0
			else:
				ax4ori = 0.25
			# CHH
			p1 = patches.Rectangle(
									(0.0, ax4ori + 0.1), 
									0.2, 
									0.2, # height of the patch
									clip_on = False,
									lw = 0,
									fill = True,
									color = 'red',
									alpha = 0.8,
									)
			# CHG
			p2 = patches.Rectangle(
									(0.0, ax4ori + 0.35), 
									0.2, 
									0.2, # height of the patch
									clip_on = False,
									lw = 0,
									fill = True,
									color = 'lime',
									alpha = 0.8,
									)
			# CpG
			p3 = patches.Rectangle(
									(0.0, ax4ori + 0.60), 
									0.2, 
									0.2, # height of the patch
									clip_on = False,
									lw = 0,
									fill = True,
									color = 'blue',
									alpha = 0.8,
									)		
			for p in [p1, p2, p3]:
				ax4.add_patch(p)

			# set font in ax4 legend
			# params = {'mathtext.default': 'regular' }  
			# plt.rcParams.update(params)

			# font = FontProperties()
			# font.set_size(14)
			# font.set_weight('bold')
			# font.set_family('monospace')
			ax4.text(
						0.35, 
						ax4ori + 0.18,
						r'CHH',
						color = 'black',
						ha = 'left',
						va = 'center',
						fontproperties = font,
						)
			ax4.text(0.35, 
					ax4ori + 0.43,
					r'CHG',
					color = 'black',
					ha = 'left',
					va = 'center',
					fontproperties = font,
					)
			ax4.text(0.35, 
					ax4ori + 0.68,
					r'CG',
					color = 'black',
					ha = 'left',
					va = 'center',
					fontproperties = font,
					)


			frame = patches.Rectangle(
										(-0.1, ax4ori-0.04), 
										1.4, 
										0.95, # height of the patch
										clip_on = False,
										lw = 1.5,
										fill = False,
										)	
			ax4.add_patch(frame)
			ax4.set_title('Entropy from:', fontsize = 10, weight = 'bold')
			ax4.set_axis_off()

			# print self.dientropys
			# print ([0.5*i for i in list(range(1, plotlen-1))])
			ax5.set_ylim(-1, 3)
			ax5.hlines(0, 0, plotlen-1, color = 'red', linewidth = 0.3)

			ax5.bar([i + 0.5 for i in list(range(plotlen-1))], self.dientropys, linewidth = 2, color = 'red', alpha = 0.6)
			ax5.hlines(self.bg_dientropys_max, 0, plotlen-1, color = 'grey', linewidth = 0.5, linestyle = '--')
			ax5.hlines(self.bg_dientropys_min, 0, plotlen-1, color = 'grey', linewidth = 0.5, linestyle = '--')
			
			ax5.set_yticks([-1, 0, 1, 2, 3])
			ax5.set_yticklabels([abs(i) for i in [-1, 0, 1, 2, 3]])
			ax5.spines['top'].set_linewidth(0)
			ax5.spines['right'].set_linewidth(0)
			ax5.spines['left'].set_linewidth(3)
			ax5.spines['bottom'].set_linewidth(0)

			seaborn.despine(ax = ax5, offset = 2, trim = True)
			ax5.tick_params(
							direction = 'out', 
							length = 4, 
							width = 3, 
							labelsize = 18,#'x-large', 
							top = False, 
							right = False, 
							bottom = False)
			for label in ax5.yaxis.get_ticklabels():
				label.set_fontweight('bold')			
			ax5.axes.get_xaxis().set_visible(False)

			# ax5.text(	
			# 			0.5, 
			# 			3.0, 
			# 			'dimer depleted', 
			# 			ha = 'left', 
			# 			va = 'center',
			# 			color = 'gray',
			# 			fontsize = 10,
			# 			weight = 'bold',
			# 			)
			# ax5.text(	
			# 			0.5, 
			# 			-1.0, 
			# 			'dimer enriched', 
			# 			ha = 'left', 
			# 			va = 'center',
			# 			color = 'gray',
			# 			fontsize = 10,
			# 			weight = 'bold',
			# 			)


			ax5.text(
						-0.5, 
						3,
						r'+ dimer depleted',
						color = 'black',
						ha = 'left',
						va = 'center',
						fontproperties = font,
				)
			ax5.text(
						-0.5, 
						-1,
						r'- dimer enriched',
						color = 'black',
						ha = 'left',
						va = 'center',
						fontproperties = font,
				)

			ax6.set_axis_off()
			# ax6.hlines(0, 0, 1, linewidth = 0.3, color = 'black')
			# ax6.hlines(self.bg_dientropys_max, 0, 1, linewidth = 0.5, color = 'red')
			# ax6.hlines(self.bg_dientropys_min, 0, 1, linewidth = 0.5, color = 'red')
			
			ax6.text(
						0.0, 
						self.bg_dientropys_max,
						r'max = ' + str(round(self.bg_dientropys_max, 2)),
						color = 'grey',
						ha = 'center',
						va = 'center',
						alpha = 0.6,
						fontsize = 10,
						# fontproperties = font,
				)	
			ax6.text(
						0.0, 
						self.bg_dientropys_min,
						r'min = ' + str(round(self.bg_dientropys_min, 2)),
						color = 'grey',
						ha = 'center',
						va = 'center',
						alpha = 0.6,
						fontsize = 10,
						# fontproperties = font,
				)			
			# ax6.tick_params(
			# 					direction = 'out', 
			# 					length = 0, 
			# 					width = 3, 
			# 					labelsize = 18, #'x-large', 
			# 					top = False, 
			# 					left = False
			# 					)


			ax1.set_yticks(y_axis)
			ax1.set_yticklabels([str(int(i)) for i in y_axis])
			ax1.spines['top'].set_linewidth(0)
			ax1.spines['right'].set_linewidth(0)
			ax1.spines['left'].set_linewidth(3)
			ax1.spines['bottom'].set_linewidth(3)
			
			seaborn.despine(ax = ax1, offset = 2, trim = True)
			for label in ax1.xaxis.get_ticklabels():
				label.set_fontweight('bold')
			for label in ax1.yaxis.get_ticklabels():
				label.set_fontweight('bold')		
			ax1.tick_params(
								direction = 'out', 
								length = 4, 
								width = 3, 
								labelsize = 18, #'x-large', 
								top = False, 
								right = False
								)
			
			ax1.set_ylabel(
							'Bits', 
							fontsize = 18, 
							weight = 'bold'
							)
			ax1.set_title(
							TF, 
							fontsize = 16, 
							weight = 'bold'
							)

			return ax1, ax2, ax3, ax4, ax5

		else:
			ax1 = self.fig.add_subplot(111)

			x_axis = list(range(plotlen))
			ax1.set_xticks(x_axis)
			ax1.set_xticklabels([str(i+1) for i in x_axis])
			
			y_axis = list(range(int(Height) + 1))
		

			fontT = FontProperties()
			fontT.set_size(70)
			fontT.set_weight('bold')
			fontT.set_family('monospace')

			fontC = FontProperties()
			fontC.set_size(68)
			fontC.set_weight('bold')
			fontC.set_family('monospace')

			bbox_props = dict(boxstyle = "square, pad = 0.0", fill = 0, lw = 0.0, alpha = 0.5)			
			# base_heights = zip(self.base_heights, self.Cmethyls, self.Gmethyls)
			base_heights = zip(self.base_heights, self.Cmethyls, self.Gmethyls)#[2:motiflen-2]
			for i, j in enumerate(base_heights):
				xshift = 0
				yshift = 0
				Bottom = 0
				width = 1

				for (base, score) in j[0]:
					
					ax1.bar(
							x_axis[i], 
							score,
							bottom = Bottom,
							width = width,
							color = COLOR_SCHEME[base],
							alpha = 0.0
							)

					if base in ['C', 'G']:
						yshift = 1.0
						txt1 = ax1.text(
										x_axis[i],
										Bottom,
										base,
										fontproperties = fontC,
										transform = transforms.offset_copy(
						           											ax1.transData, 
						           											fig = self.fig,
						           											x = -6,
						           											y = yshift*(score), 
						           											units = 'points'
						           										),
										ha = 'center',
										va = 'baseline',
										color = COLOR_SCHEME[base],
										bbox = bbox_props
										)
						txt1.set_path_effects([Scale(1.2, score)])
					else:
						txt1 = ax1.text(
										x_axis[i],
										Bottom,
										base,
										fontproperties = fontT,
										ha = 'center',
										va = 'baseline',
										color = COLOR_SCHEME[base],
										bbox = bbox_props
										)
						txt1.set_path_effects([Scale(1.0, score)])

					Bottom += score


			
			ax1.set_yticks(y_axis)
			ax1.set_yticklabels([str(i) for i in y_axis])
			ax1.spines['top'].set_linewidth(0)
			ax1.spines['right'].set_linewidth(0)
			ax1.spines['left'].set_linewidth(3)
			ax1.spines['bottom'].set_linewidth(3)
			
			seaborn.despine(ax = ax1, offset = 2, trim = True)
			for label in ax1.xaxis.get_ticklabels():
				label.set_fontweight('bold')
			for label in ax1.yaxis.get_ticklabels():
				label.set_fontweight('bold')		
			ax1.tick_params(
								direction = 'out', 
								length = 4, 
								width = 3, 
								labelsize = 16,#'x-large', 
								top = False, 
								right = False
								)
			
			ax1.set_ylabel(
							'Bits', 
							fontsize = 16, 
							weight = 'bold'
							)
			ax1.set_title(
							tissue + '_' + TF, 
							fontsize = 18, 
							weight = 'bold'
							)
			ax1.text(
						-1.1, 
						-0.5, 
						'MethylSeqLogo', 
						ha = 'center', 
						va = 'bottom',
						fontsize = 12,
						weight = 'bold',
						)
			return ax1,
#-----------------------------------------------------------

class riverLake:
	def __init__(self, fig, tissue, fourletterppm, dippm, Cmethyls, Gmethyls, bgpps, J_bCG, J_bCHG, J_bCHH, Freqs_):
		self.fig = fig
		self.tissue = tissue
		self.fourletterppm = fourletterppm
		self.dippm = dippm
		self.Cmethyls = Cmethyls
		self.Gmethyls = Gmethyls
		self.bgpps = bgpps
		self.J_bCG = J_bCG
		self.J_bCHG = J_bCHG
		self.J_bCHH = J_bCHH
		self.Freqs_ = Freqs_

	def plotRiverLake(self):
	
		""" Plot methylriverlake """

		# remove axes content
		self.fig.clf()
		self.fig.tight_layout()

		Height = 3.0

		if (mode == 'Methyl'):
			gs = GridSpec(
							1, 
							2, 
							width_ratios=[motiflen-1, 1], 
							height_ratios = [1], 
							wspace = 0.0, 
							hspace = 0.0,
							)
			ax1 = plt.subplot(gs[0])
			ax2 = plt.subplot(gs[1])

			x_axis = list(range(plotlen))
			ax1.set_xticks(x_axis)
			ax1.set_xticklabels([str(i+1) for i in x_axis])
			ax1.set_ylim(-0.2, 1.2)
			
			bbox_props = dict(boxstyle = "square, pad = 0", fill = 0.0, lw = 0.0, alpha = 0.5)

			# convert pandas dataframe to numpy array with decimal = 1
			mfourletterppm = np.array(self.fourletterppm)
			mfourletterppm = np.round(mfourletterppm, 1)

			base_heights = zip(mfourletterppm, self.Cmethyls, self.Gmethyls)
			for i, j in enumerate(base_heights):

				# # xshift = 0
				# print i
				# print j
				yshift = 4
				Bottom = 0
				width = 1.0
				yindex = [0.0, 0.25, 0.5, 0.75]
				for (base, score) in zip(['A', 'C', 'G', 'T'], j[0]):
					ax1.bar(
							x_axis[i], 
							0.25,
							bottom = Bottom,
							width = width,
							color = COLOR_SCHEME[base],
							alpha = 0.0
							)

					if score > 0.00:
			
						oval = patches.Ellipse(
												xy = (x_axis[i], Bottom),
												width = 0.7 * score,
												height = 0.4 * score,
												color = 'lightgrey',
												alpha = 1.0,
												fill = True,
												zorder = 4
												)
						ax1.add_patch(oval)
						
						if i < (motiflen - 1):
							for k, (base1, score1) in enumerate(zip(['A', 'C', 'G', 'T'], mfourletterppm[i + 1])):
							
								if score1 > 0.00:
			
									index = base + base1
									ax1.plot([i, i+1], [Bottom, yindex[k]], lw = 25.0 * self.dippm[i][index], color = 'lightgrey', zorder = 2)

									if self.dippm[i][index] >= (score*score1):
										scale1 = self.dippm[i][index]/(score*score1)
										aline = ax1.plot([i, i+1], [Bottom, yindex[k]], lw = 1*scale1, color = 'navy', zorder = 3)
									else:
										pass

						# set font
						fontC = FontProperties()
						fontC.set_size(48 * score) # scale to probability
						fontC.set_weight('bold')
						fontC.set_family('monospace')

						txt1 = ax1.text(
										x_axis[i],
										Bottom,
										base,
										fontproperties = fontC,
										transform = transforms.offset_copy(
																			ax1.transData, 
																			fig = self.fig,
																			x = 0,
																			y = -yshift*score, 
																			units = 'points'
																			),
										ha = 'center',
										va = 'center',
										color = COLOR_SCHEME[base],
										bbox = bbox_props,
										zorder = 4,
										)
						if base in ['C', 'G']:
							txt2 = ax1.text(
											x_axis[i],
											Bottom,
											base,
											fontproperties = fontC,
											transform = transforms.offset_copy(
																				ax1.transData, 
																				fig = self.fig,
																				x = 0,
																				y = -yshift*score, 
																				units = 'points'
																				),
											ha = 'center',
											va = 'center',
											color = 'black',
											clip_on = True,
											bbox = bbox_props,
											zorder = 4,
											)
							txt3 = ax1.text(
											x_axis[i],
											Bottom,
											base,
											fontproperties = fontC,
											transform = transforms.offset_copy(
																				ax1.transData, 
																				fig = self.fig,
																				x = 0,
																				y = -yshift*score, 
																				units = 'points'
																				),
											ha = 'center',
											va = 'top',
											color = 'black',
											clip_on = True,
											alpha = 0.0,
											bbox = bbox_props,
											zorder = 4,
											)
							txt4 = ax1.text(
											x_axis[i],
											Bottom,
											base,
											fontproperties = fontC,
											transform = transforms.offset_copy(
																				ax1.transData, 
																				fig = self.fig,
																				x = 0,
																				y = -yshift*score, 
																				units = 'points'
																				),
											ha = 'center',
											va = 'baseline',
											color = 'black',
											clip_on = True,
											alpha = 0.0,
											bbox = bbox_props,
											zorder = 4,
											)

							self.fig.canvas.draw()
							bound1 = txt3.get_window_extent()
							bound2 = txt4.get_window_extent()

							inv = ax1.transData.inverted()
							a1 = inv.transform((bound1.xmin,bound1.ymin))
							a2 = inv.transform((bound1.xmax,bound1.ymax))
							a3 = inv.transform((bound2.xmin,bound2.ymin))
							a4 = inv.transform((bound2.xmax,bound2.ymax))
							
							letterheight = a4[1] - a2[1]
							
							left = x_axis[i] - 0.5
							halfletter = letterheight * 0.50
							
							bot = Bottom - halfletter

							bgMlevel = None
							foreMlevel = None
							if base == 'C':
								bgMlevel = self.Freqs_.iloc[i]['CpG_p'] * self.J_bCG + self.Freqs_.iloc[i]['CHG_p'] * self.J_bCHG + self.Freqs_.iloc[i]['CHH_p'] * self.J_bCHH
								foreMlevel = self.Freqs_.iloc[i]['CpG_p'] * j[1][0] + self.Freqs_.iloc[i]['CHG_p'] * j[1][1] + self.Freqs_.iloc[i]['CHH_p'] * j[1][2]
								
								# methylation = letterheight * j[1]
							elif base == 'G':
								bgMlevel = self.Freqs_.iloc[i]['CpG_m'] * self.J_bCG + self.Freqs_.iloc[i]['CHG_m'] * self.J_bCHG + self.Freqs_.iloc[i]['CHH_m'] * self.J_bCHH
								foreMlevel = self.Freqs_.iloc[i]['CpG_m'] * j[2][0] + self.Freqs_.iloc[i]['CHG_m'] * j[2][1] + self.Freqs_.iloc[i]['CHH_m'] * j[2][2]
								# methylation = letterheight * j[2]
							print (i, bot, base, letterheight, foreMlevel, bgMlevel)

							p = patches.Rectangle(
			    									(left, bot), 
			    									width,
			    									letterheight * foreMlevel, # height of the patch
			    									clip_on = True,
			    									lw = 0,
			    									fill = False,
			    									)					
							ax1.add_patch(p)
							txt2.set_clip_path(p)
							if letterheight >= 0.1:						
								ax1.plot(
											(i-0.3, i+0.3),
											(bot + letterheight * bgMlevel, bot + letterheight * bgMlevel),
											linestyle = '--',
											color = 'grey',
											linewidth = 1.0,
											zorder = 5, 
										)
						
					Bottom += 0.25

				# add key to logo
				fontKey = FontProperties()
				fontKey.set_size(22)
				fontKey.set_weight('bold')
				fontKey.set_family('monospace')
				
				ax2top = int(Height)
				methylkeyori = ax2top - 1
				
				# ax2.set_xlim(0.0, 1.0*(1+motiflen)/10)
				ax2.set_xlim(0.0, 1.2)
				ax2.set_ylim(0.0, ax2top)

				# y_axis = range(4)
				# ax2.set_yticks(y_axis)
				# ax2.set_yticklabels([str(i) for i in y_axis])
				# params = {'mathtext.default': 'regular' }  
				# plt.rcParams.update(params)			

				CGC = ax2.text(0.2, methylkeyori + 0.65, 'C', color = 'white', ha = 'center', fontproperties = fontKey,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				CGG = ax2.text(0.5, methylkeyori + 0.65, 'G', color = 'white', ha = 'center', fontproperties = fontKey,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				cgc = ax2.text(0.2, methylkeyori + 0.65, 'C', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				cgg = ax2.text(0.5, methylkeyori + 0.65, 'G', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)

				for txt in [CGC, CGG, cgc, cgg]:
					txt.set_path_effects([matplotlib.patheffects.Stroke(linewidth=2, foreground='black'), matplotlib.patheffects.Normal()])
					

				p1 = patches.Rectangle(
										(0.0, methylkeyori + 0.65), 
										1.0, 
										self.J_bCG*0.33, # height of the patch
										clip_on = True,
										lw = 0,
										fill = False
										)					
				ax2.add_patch(p1)
				cgc.set_clip_path(p1)
				cgg.set_clip_path(p1)


				CHGC = ax2.text(0.2, methylkeyori + 0.325, 'C', color = 'white', ha = 'center', fontproperties = fontKey, 
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				CHGH = ax2.text(0.5, methylkeyori + 0.325, 'H', color = 'white', ha = 'center', fontproperties = fontKey,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				CHGG = ax2.text(0.8, methylkeyori + 0.325, 'G', color = 'white', ha = 'center', fontproperties = fontKey,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)

				chgc = ax2.text(0.2, methylkeyori + 0.325, 'C', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				chgh = ax2.text(0.5, methylkeyori + 0.325, 'H', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				chgg = ax2.text(0.8, methylkeyori + 0.325, 'G', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)

				for txt in [CHGC, CHGH, CHGG, chgc, chgh, chgg]:
					txt.set_path_effects([matplotlib.patheffects.Stroke(linewidth=2, foreground='black'), matplotlib.patheffects.Normal()])

				p2 = patches.Rectangle(
										(0.0, methylkeyori + 0.325), 
										1.0, 
										self.J_bCHG*0.33, # height of the patch
										clip_on = True,
										lw = 0,
										fill = False
										)					
				ax2.add_patch(p2)
				chgc.set_clip_path(p2)
				chgh.set_clip_path(p2)
				chgg.set_clip_path(p2)

				CHHC = ax2.text(0.2, methylkeyori, 'C', color = 'white', ha = 'center', fontproperties = fontKey, 
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				CHHH1 = ax2.text(0.5, methylkeyori, 'H', color = 'white', ha = 'center', fontproperties = fontKey,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				CHHH2 = ax2.text(0.8, methylkeyori, 'H', color = 'white', ha = 'center', fontproperties = fontKey,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)

				chhc = ax2.text(0.2, methylkeyori, 'C', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				chhh1 = ax2.text(0.5, methylkeyori, 'H', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)
				chhh2 = ax2.text(0.8, methylkeyori, 'H', color = 'black', ha = 'center', fontproperties = fontKey, clip_on = True,
							   transform = transforms.offset_copy(
								           							ax2.transData, 
								           							fig = self.fig,
								           							x = 0.0,
								           							y = 1.5, 
								           							units = 'points'
								           										),)

				for txt in [CHHC, CHHH1, CHHH2, chhc, chhh1, chhh2]:
					# txt.set_path_effects([Scale(1.0, self.bgpps[nt])])
					txt.set_path_effects([matplotlib.patheffects.Stroke(linewidth=2, foreground='black'), matplotlib.patheffects.Normal()])

				p3 = patches.Rectangle(
										(0.0, methylkeyori), 
										1.0, 
										self.J_bCHH*0.33, # height of the patch
										clip_on = True,
										lw = 0,
										fill = False
										)					
				ax2.add_patch(p3)
				chhc.set_clip_path(p3)
				chhh1.set_clip_path(p3)
				chhh2.set_clip_path(p3)


				# frequency key
				freqkeyori = ax2top - 3.0

				bot = freqkeyori + 0.2
				for nt in sorted(['A', 'C', 'G', 'T'], reverse = True):
					freq = round(self.bgpps[nt], 4)
					bar = ax2.bar(0.5, freq, bottom = bot, width = 1.0, color = COLOR_SCHEME[nt], alpha = 0.0)
					oval = patches.Ellipse(
												xy = (0.5, bot),
												# radius = 1.0 * freq * 0.5,
												width = 1.0 * freq,
												height = 0.9 * freq,
												color = 'lightgrey',
												alpha = 1.0,
												fill = True,
												clip_on = False,
												zorder = 3,
												)
					ax2.add_patch(oval)

					fontKey2 = FontProperties()
					fontKey2.set_size(50 * self.bgpps[nt])
					fontKey2.set_weight('bold')
					fontKey2.set_family('monospace')
					
					txt = ax2.text(
										0.5, 
										bot, 
										nt, 
										ha = 'center', 
										va = 'center', 
										fontproperties = fontKey2,
										color = COLOR_SCHEME[nt],
										zorder = 4,
										)
					if nt in ['C', 'G']:
						txt2 = ax2.text(
											0.5, 
											bot, 
											nt, 
											ha = 'center', 
											va = 'center', 
											fontproperties = fontKey2,
											color = 'black',
											clip_on = True,
											zorder = 4,
											)
						txt3 = ax2.text(
											0.5, 
											bot, 
											nt, 
											ha = 'center', 
											va = 'top', 
											fontproperties = fontKey2,
											color = 'black',
											alpha = 0.0,
											)
						txt4 = ax2.text(
											0.5, 
											bot, 
											nt, 
											ha = 'center', 
											va = 'baseline', 
											fontproperties = fontKey2,
											color = 'black',
											alpha = 0.0,
											)
						self.fig.canvas.draw()
						bound1 = txt3.get_window_extent()
						bound2 = txt4.get_window_extent()

						inv = ax2.transData.inverted()
						a1 = inv.transform((bound1.xmin,bound1.ymin))
						a2 = inv.transform((bound1.xmax,bound1.ymax))
						a3 = inv.transform((bound2.xmin,bound2.ymin))
						a4 = inv.transform((bound2.xmax,bound2.ymax))
							
						letterheight = a4[1] - a2[1]
						bgMlevel = None
						bgMlevel = self.bgpps['CpG'] * self.J_bCG + self.bgpps['CHG'] * self.J_bCHG + self.bgpps['CHH'] * self.J_bCHH
						halfletterheight = letterheight/2.0
	
						pb = patches.Rectangle(
												(0.0, bot - halfletterheight),
												1.0,
												bgMlevel*letterheight,
												clip_on = True,
												lw = 0.0,
												fill = False,
												)
						ax2.add_patch(pb)
						txt2.set_clip_path(pb)
						ax2.plot(
									(0.3, 0.7),
									(bot - halfletterheight + bgMlevel*letterheight, bot - halfletterheight + bgMlevel*letterheight),
									linestyle = '--',
									linewidth = 0.5,
									color = 'grey',
									zorder = 5,
									)

					bot = bot + 0.25*2

				p31 = patches.Rectangle(
										(-0.1, freqkeyori-0.05), 
										1.2, 
										3.1, # height of the patch
										clip_on = False,
										lw = 1.2,
										fill = False
										)
				ax2.add_patch(p31)
				ax2.set_axis_off()
				ax2.text(	
							0.5, 
							-0.5, 
							'MethylSeqLogo', 
							ha = 'center', 
							va = 'bottom',
							fontsize = 10,
							weight = 'bold',
							)

			ax1.spines['top'].set_linewidth(0)
			ax1.spines['right'].set_linewidth(0)
			ax1.spines['left'].set_linewidth(0)
			ax1.spines['bottom'].set_linewidth(2)

			seaborn.despine(ax = ax1, offset = 2, trim = True)		
			for label in ax1.xaxis.get_ticklabels():
				label.set_fontweight('bold')

			ax1.get_yaxis().set_visible(False)
			ax1.tick_params(direction='out', length=4, width=2, labelsize= 'x-large', 
							top=False, right=False, left = False)
			# ax1.text(motiflen, -0.4, 'MethylSeqLogo', ha = 'center', va = 'bottom', fontsize = 10)
			# ax1.set_ylabel('Nucleotide', fontsize = 14, weight = 'bold')
			ax1.set_title(TF, fontsize = 16, weight = 'bold')

			return ax1, ax2,
		

		else:

			ax1 = self.fig.add_subplot(111)
			x_axis = list(range(plotlen))
			ax1.set_xticks(x_axis)
			ax1.set_xticklabels([str(i+1) for i in x_axis])
			ax1.set_ylim(-0.2, 1.2)
			
			# y_axis = range(int(Height) + 1)

			bbox_props = dict(boxstyle = "square, pad = 0", fill = 0.0, lw = 0.0, alpha = 0.5)

			# convert pandas dataframe to numpy array with decimal = 1
			mfourletterppm = np.array(self.fourletterppm)
			mfourletterppm = np.round(mfourletterppm, 1)

			# base_heights = zip(mfourletterppm, self.Cmethyls, self.Gmethyls)
			for i, j in enumerate(mfourletterppm):
				print (i, j)
				# xshift = 0
				yshift = 4
				Bottom = 0
				width = 1.0
				yindex = [0.0, 0.25, 0.5, 0.75]
				for (base, score) in zip(['A', 'C', 'G', 'T'], j):
					ax1.bar(
							x_axis[i], 
							0.25,
							bottom = Bottom,
							width = width,
							color = COLOR_SCHEME[base],
							alpha = 0.0
							)

					if score > 0.00:
			
						oval = patches.Ellipse(
												xy = (x_axis[i], Bottom),
												width = 0.7 * score,
												height = 0.4 * score,
												color = 'lightgrey',
												alpha = 1.0,
												fill = True,
												zorder = 3
												)
						ax1.add_patch(oval)
						
						if i < (motiflen - 1):
							for k, (base1, score1) in enumerate(zip(['A', 'C', 'G', 'T'], mfourletterppm[i + 1])):
							
								if score1 > 0.00:
			
									index = base + base1
									ax1.plot([i, i+1], [Bottom, yindex[k]], lw = 25.0 * self.dippm[i][index], color = 'lightgrey', zorder = 1)

									if self.dippm[i][index] >= (score*score1):
										scale1 = self.dippm[i][index]/(score*score1)
										aline = ax1.plot([i, i+1], [Bottom, yindex[k]], lw = 1*scale1, color = 'navy', zorder = 1)
									else:
										pass

						# set font
						fontC = FontProperties()
						fontC.set_size(48 * score) # scale to probability
						fontC.set_weight('bold')
						fontC.set_family('monospace')

						txt1 = ax1.text(
										x_axis[i],
										Bottom,
										base,
										fontproperties = fontC,
										transform = transforms.offset_copy(
																			ax1.transData, 
																			fig = self.fig,
																			x = 0,
																			y = -yshift*score, 
																			units = 'points'
																			),
										ha = 'center',
										va = 'center',
										color = COLOR_SCHEME[base],
										bbox = bbox_props
										)
					Bottom += 0.25
			ax1.spines['top'].set_linewidth(0)
			ax1.spines['right'].set_linewidth(0)
			ax1.spines['left'].set_linewidth(0)
			ax1.spines['bottom'].set_linewidth(2)

			seaborn.despine(ax = ax1, offset = 2, trim = True)		
			for label in ax1.xaxis.get_ticklabels():
				label.set_fontweight('bold')

			ax1.get_yaxis().set_visible(False)
			ax1.tick_params(
							direction='out', 
							length=4, 
							width=2, 
							labelsize= 16,#'x-large', 
							top=False, 
							right=False, 
							left = False)
			ax1.text(
						motiflen-1, 
						-0.6, 
						'MethylSeqLogo', 
						ha = 'center', 
						va = 'bottom',
						fontsize = 12,
						weight = 'bold',
						)
			# ax1.set_ylabel('Nucleotide', fontsize = 14, weight = 'bold')
			ax1.set_title(TF, fontsize = 18, weight = 'bold')


			return ax1,

#-----------------------------------------------------------

def main():

	# logging.basicConfig(filename="methylseqlogo.log", level = logging.DEBUG, format = '%(asctime)s, %(name)s-%(levelname)-s: %(message)s')
	parser = get_parser()
	args = parser.parse_args()

	global dir_path
	dir_path = os.path.dirname(os.path.realpath(__file__))
	print (dir_path)
	# seqfile = args.inputfasta
	global TF
	TF = args.transcriptionfactor  

	global species
	species = args.species

	global tissue
	tissue = args.tissuetypes

	global mode
	mode = args.mode

	global logotype
	logotype = args.logotype

	global region
	region = args.regionofinterest

	global threshold
	threshold = float(args.threshold)

	global spanwindowleft
	spanwindowleft = args.spanwindowleft

	global plotlen
	plotlen = args.plotlen


	# print dir_path
	pd.set_option('display.float_format', lambda x: '%.2f' % x)

	species_tissue_pair = {
							'human' : ['H1-hESC', 'HepG2', 'K562', 'GM12878', 'HeLa-S3', 'naive-hESC', 'primed-hESC'], 
							'mouse' : ['mESC', 'mMPGC_E13p5', 'mMPGC_E16p5', 'mFPGC_E13p5', 'mFPGC_E16p5'], 
							'arabidopsis' : ['Leaf', 'Inflorescence', 'Root'], 
							'maize' : ['root', 'shoot']
							}

	if tissue not in species_tissue_pair[species]:
		print (tissue + " is not in" + " " + species + "'s tissue database.")
		print (tissue + "不在" + species + "的組織資料庫內。")
		print (tissue + "は" + species + "の組織データベースにはありませんでした。")
		print ("Job cancelled." + "工作終止。" + "ジョブがキャンセルされました。")
		raise SystemExit(0)

	print ("\n")
	if logotype in ['Shannon', 'Kullback-Liebler']:
		print ("Plotting " + TF + ' ' + mode + ' ' + logotype + ' logo of ' + species + ' ' + tissue + ' ' + region + ' background.')
	elif logotype == 'riverlake':
		print ("Plotting " + TF + ' ' + mode + ' ' + logotype + ' of ' + species + ' ' + tissue + '.')
	else:
		print ("Plotting " + TF + ' ' + mode + ' all logos of ' + species + ' ' + tissue + '.')
	print ("\n")

	global nucleotides
	nucleotides = ['A', 'C', 'G', 'T']

	global COLOR_SCHEME
	COLOR_SCHEME = {
						'G': 'orange', 
	            		'A': 'green', 
	            		'C': 'deepskyblue', 
	            		'T': 'red'
	 				}
	global BAR_COLOR_SCHEME

	BAR_COLOR_SCHEME = {
						'CG': 'black', 
	            		'CHG': 'gray', 
	            		'CHH': 'lightgray',
	 				}

	seqfile = None
	ctxfile = None
	creaddata = None
	treaddata = None
	if region in ['whole_genome', 'promoter']:
		seqfile = TF + '_' + tissue + '_' + region + '_binding_sites_seq.fa' # binding site .fasta
		ctxfile = TF + '_' + tissue + '_' + region + '_binding_sites_ctx.txt'
		creadfile = TF + '_' + tissue + '_' + region + '_binding_sites_cread.txt'
		treadfile = TF + '_' + tissue + '_' + region + '_binding_sites_tread.txt'
	elif region in ['uniform']:
		seqfile = TF + '_' + tissue + '_' + 'whole_genome' + '_binding_sites_seq.fa'
		ctxfile = TF + '_' + tissue + '_' + 'whole_genome' + '_binding_sites_ctx.txt'
		creadfile = TF + '_' + tissue + '_' + 'whole_genome' + '_binding_sites_cread.txt'
		treadfile = TF + '_' + tissue + '_' + 'whole_genome' + '_binding_sites_tread.txt'

	seqdata, ctxdata, creaddata, treaddata = datainput(seqfile, ctxfile, creadfile, treadfile)

	# Determine plotting window
	# Default setting (0, motiflen)
	global motiflen
	motiflen = len(seqdata.columns)
	print (TF + " binding motif is " + str(motiflen) + "bp")

	if plotlen is None:
		plotlen = motiflen - spanwindowleft + 1
	else:
		pass

	global endpos
	if (spanwindowleft + plotlen > motiflen):
		endpos = motiflen
		print ("Warning: user defined plotting length is over motif length" + '\n')
		print ("Plotting " + TF + " binding motif from pos " + str(spanwindowleft) + " to pos " + str(motiflen))
	else:
		endpos = spanwindowleft + plotlen - 1
		print ("Plotting " + TF + " binding motif from pos " + str(spanwindowleft) + " to pos " + str(endpos))


	print("\n")
	seqdata = seqdata.iloc[:, spanwindowleft-1:endpos]
	ctxdata = ctxdata.iloc[:, spanwindowleft-1:endpos]
	creaddata = creaddata.iloc[:, spanwindowleft-1:endpos]
	treaddata = treaddata.iloc[:, spanwindowleft-1:endpos]

	global pseudocount
	pseudocount = 1.0

	pfm, ppm = seqToPFM(seqdata, 1.0)
	# JiCs, PiCs, Cmethyls, Gmethyls = MethylProbability(ctxdata, creaddata, treaddata, J_bCG, J_bCHG, J_bCHH)

	bgprobtable = species + '_' + tissue + '_probability.txt'
	# print (bgprobtable)
	bgpps, J_bCG, J_bCHG, J_bCHH = readProbTable(bgprobtable, region)
	JiCs, PiCs, Cmethyls, Gmethyls, Freqs_ = MethylProbability(ctxdata, creaddata, treaddata, J_bCG, J_bCHG, J_bCHH)

	if logotype in ['Kullback-Liebler', 'Shannon']:
		Cents = methylationEntropy(JiCs, PiCs, J_bCG, J_bCHG, J_bCHH, logotype)
		entropys = totalEntropy(ppm, bgpps, Cents, logotype)
		four_base_heights = perBaseEntropy(entropys, ppm)
		dippm = to4basedippm(seqdata)
		dientropys, bg_dientropys_max, bg_dientropys_min = twomerBg(bgpps, dippm)
		fig = set_fig(entropys)
		plotobj = seqLogoPlot(fig, tissue, four_base_heights, entropys, Cmethyls, Gmethyls, bgpps, dientropys, bg_dientropys_max, bg_dientropys_min, J_bCG, J_bCHG, J_bCHH, Freqs_)
		plotobj.plotlogo()
		logoname = TF + '_' + species + '_' + tissue + '_' + region + '_' + mode + '_' + logotype + '_seqlogo.png'
		plt.savefig(dir_path + '/../Output/' + logoname, bbox_inches = 'tight', dpi = 600)
		print (logoname + ' is saved in' + dir_path + '/../Output/.')
	elif logotype == 'riverlake':
		dippm = to4basedippm(seqdata)
		Cents = methylationEntropy(JiCs, PiCs, J_bCG, J_bCHG, J_bCHH, logotype)
		entropys = totalEntropy(ppm, bgpps, Cents, logotype)
		four_base_heights = perBaseEntropy(entropys, ppm)
		# fig = set_fig(entropys)
		fig = plt.figure(figsize = (plotlen+1, 3.0))
		riverlakeobj = riverLake(fig, tissue, ppm, dippm, Cmethyls, Gmethyls, bgpps, J_bCG, J_bCHG, J_bCHH, Freqs_)
		riverlakeobj.plotRiverLake()
		logoname = TF + '_' + species + '_' + tissue + '_' + region + '_' + mode + '_' + logotype + '_seqlogo_bar7.pdf'
		plt.savefig(dir_path + '/../Output/' + logoname, bbox_inches = 'tight', dpi = 600)
		print (logoname + ' is saved in' + dir_path + '/../Output/.')
	elif logotype == 'all':
		for i in ['Kullback-Liebler', 'Shannon']:
			# Cents = methylationEntropy(JiCs, PiCs, J_bCG, J_bCHG, J_bCHH, logotype)
			Cents = methylationEntropy(JiCs, PiCs, J_bCG, J_bCHG, J_bCHH, i)
			entropys = totalEntropy(ppm, bgpps, Cents, i)
			# entropys = totalEntropy(ppm, bgpps, Cents, logotype)
			four_base_heights = perBaseEntropy(entropys, ppm)
			fig = set_fig(entropys)
			plotobj = seqLogoPlot(fig, tissue, four_base_heights, entropys, Cmethyls, Gmethyls, bgpps, J_bCG, J_bCHG, J_bCHH, Freqs_)
			plotobj.plotlogo()
			logoname = TF + '_' + species + '_' + tissue + '_' + region + '_' + mode + '_' + i + '_seqlogo_bar7.pdf'
			plt.savefig(dir_path + '/../Output/' + logoname, bbox_inches = 'tight', dpi = 600)
			print (logoname + ' is saved in ./Output/.')
		dippm = to4basedippm(seqdata)
		dientropys = twomerBg(bgpps, dippm)
		Cents = methylationEntropy(JiCs, PiCs, J_bCG, J_bCHG, J_bCHH, 'riverlake')
		entropys = totalEntropy(ppm, bgpps, Cents, 'riverlake')
		four_base_heights = perBaseEntropy(entropys, ppm)
		# fig = set_fig(entropys)

		fig = plt.figure(figsize = (plotlen, 3.0))
		riverlakeobj = riverLake(fig, tissue, ppm, dippm, Cmethyls, Gmethyls, bgpps, J_bCG, J_bCHG, J_bCHH, Freqs_)
		riverlakeobj.plotRiverLake()
		logoname = TF + '_' + species + '_' + tissue + '_' + region + '_' + mode + '_' + 'riverlake' + '_seqlogo_bar7.png'
		plt.savefig(dir_path + '/../Output/' + logoname, bbox_inches = 'tight', dpi = 600)
		print (logoname + ' is saved in ./Output/.')	

	print ("\n")

main()
# if __name__ == '__main__':
# main()
	# if len(df.index) < 100:
	# 	print "Input with less then 100 sites!"
	# 	print "\n"


