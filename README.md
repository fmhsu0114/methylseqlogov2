# MethylSeqLogoV2

Transcription factors (TFs) bind to short DNA sequence motifs and initiate gene transcription. 
DNA methylation as a nucleotide modification can regulate TF binding efficacy. 
Over the past several decades, a sequence logo is frequently used to show sequence conservation of a TF binding motif. 
By incorporating bisulfite sequencing data, we invent a chimeric sequence logo showing the sequence conservation along with the information content of DNA methylation per base.


<!-- ![MethylSeqLogo_pipeline_05282022](/uploads/365adae41949ff7853e2c080a022606b/MethylSeqLogo_pipeline_05282022.png) -->

<figure>
    <img src="https://gitlab.com/fmhsu0114/methylseqlogov2/uploads/365adae41949ff7853e2c080a022606b/MethylSeqLogo_pipeline_05282022.png" width=100%>
</figure>


**Requirements**

_OS_
1. Linux Ubuntu 16.04.4
2. Windows 10 Build 17134
3. macOS 12.4

_Python 3.9_
1. matplotlib 3.3.4
2. numpy 1.20.1
3. pandas 1.2.2
4. seaborn 0.11.1
5. biopython 1.78

<!-- _Other_
1. [ImageMagick](https://imagemagick.org/script/download.php) -->

**Installation**

*We recommend using virtualenv to set up environments with required packages*

1. Obtain [Python 3.9](https://www.python.org/) and [virtualenv](https://virtualenv.pypa.io/en/stable/)
2. Create a virtual environment and then activate it

    ```
    $ virtualenv -p python3.9 MethylSeqLogo_env
    $ source ~/MethylSeqLogo_env/bin/activate
    ```
    
3. Download the source code and install requirements

    ```
    $ git clone git@gitlab.com:fmhsu0114/methylseqlogov2.git
    $ pip install -r methylseqlogov2/requirement/requirement.txt
    ```
4. Add path/to/methylseqlogov2/ to PATH and source

*If you have environments settled, you can also install from source code*

1. Download

    ```
    $ wget M......
    $ tar zxvf M......

2. Add path/to/methylseqlogov2/ to PATH and source


**Usage**

1. MethylSeqLogo takes four processed files as inputs including:

    * seq.fa: .fasta sequence of each TFBS
    * ctx.txt: context of each TFBS (x: CG, y: CHG, z: CHH; upper case: forward strand, lower case: reverse strand)
    * cread.txt: read count as cytosine/guanine of C/G (methylated read) in each TFBS
    * tread.txt: read count as thymine/adenine of T/A (un-methylated read) in each TFBS


<!--     * BSconvertgenome: convert genome fasta sequence from 4-letter to 6-letter based on methylation calling data, e.g. ENCODE .bed file, CGmap etc.
    * grepSeq: grep motif sequence with 6-letter .fasta and TFBS .bed coordinates.
    * MethylSeqLogo: plotting sequence logo
    * Animation: plotting animation for time-lapse methylome -->

2. MethylSeqLogo module has 9 arguments

    | Argument |       Detail         |     Field       |    Options   |     Default     |
    |:---------|:---------------------|:---------------:|:-------------|:----------------|
    | -T       |--transcriptionfactor |     required    |       -      |         -       |
    | -s       |--species             |     required    |       -      |         -       |
    | -t       |--tissuetypes         |     optional    |human: H1-hESC, HepG2, K562, GM12878<br>mouse: ESC<br>Arabidopsis: Root, Leaf, Inflorescence<br>maize: shoot |human: H1-hESC<br>mouse: ESC<br>arabidopsis:root<br>maize:shoot|
    | -sp      |--spanwindowleft      |     optional    |       -      |         0       |
    | -pl      |--plotlen             |     optional    |       -      |         -       |
    | -m       |--mode                |     optional    |Methyl<br>Regular|   Methyl   |
    | -l       |--logotype            |     optional    |Shannon<br>Kullback-Liebler<br>both (Shannon, Kullback-Liebler)<br>riverlake<br>all   | Kullback-Liebler  |
    | -r       |--regionofinterest    |     optional    |whole_genome<br>promoter<br>uniform|   whole_genome  |
    | -th      |--threshold           |     optional    |       -      |         0.4     |


3. Run MethylSeqLogo

    * Plot MYC __Methyl__ __Kullback-Liebler__ seqlogo of human H1-hESC whole_genome background


    ```
    $ MethylSeqLogo MethylSeqLogo -T MYC  -s human -t H1-hESC -m Methyl -l Kullback-Liebler -r whole_genome
    ```
    
    <figure>
        <img src="https://gitlab.com/fmhsu0114/methylseqlogov2/uploads/0c79f7457a533579e1ddb0be58264ff7/MYC_human_H1-hESC_whole_genome_Methyl_Kullback-Leibler_seqlogo.png" width=100%>
    </figure>

    * Plot MYC __Methyl__ __Kullback-Liebler__ seqlogo of human H1-hESC promoter background


    ```
    $ MethylSeqLogo MethylSeqLogo -T MYC -s human -t H1-hESC -m Methyl -l Kullback-Liebler -r promoter
    ```

    <figure>
        <img src="https://gitlab.com/fmhsu0114/methylseqlogov2/uploads/53a77ab7e18304370bae651d267e1339/MYC_human_H1-hESC_promoter_Methyl_Kullback-Liebler_seqlogo.png" width=100%>
    </figure>
    
    * Plot MYC __Methyl__ __Kullback-Liebler__ MethylScape of human H1-hESC promoter background


    ```
    MethylSeqLogo MethylScape -T MYC -s human -t H1-hESC -l Kullback-Liebler -r promoter -ML 12 -FL 1000 -sm Y -bin 1
    ```

    <figure>
        <img src="https://gitlab.com/fmhsu0114/methylseqlogov2/uploads/8e4d3e15276373d8af19c157f20cea56/MYC_human_H1-hESC_promoter_neighbor_1000_bin_1_MethylScape.png" width=100%>
    </figure>


<!--     * Plot MYC __Regular__ __Shannon__ seqlogo of human H1-hESC promoter background

    ```
    $ MethylSeqLogo MethylSeqLogo -T MYC -s human -t H1-hESC -m Regular -l Shannon -r promoter
    ```

    <figure>
            <img src="https://gitlab.com/fmhsu0114/methylseqlogov2/uploads/33fb33e8acbd8f820110aacae53376f4/MYC_human_H1-hESC_promoter_Methyl_Shannon_seqlogo.png" width=100%>
    </figure> -->


<!--     * Plot MYC __Methyl__ __riverlake__ logo of human H1-hESC promoter background


    ```
    $ MethylSeqLogo MethylSeqLogo -T MYC -s human -t H1-hESC -m Methyl -l riverlake -r promoter
    ```


    <figure>
        <img src="https://gitlab.com/fmhsu0114/MethylSeqLogo/raw/master/Output/MYC_human_H1-hESC_whole_genome_Methyl_riverlake_seqlogo.png" width=100%>
    </figure>
 -->

<!-- 4. Animation module has 5 arguments

    | Argument |       Detail         |     Field       |    Options   |     Default     |
    |:---------|:---------------------|:---------------:|:-------------|:----------------|
    | -i       |--inputfile           |     required    |       -      |         -       |
    | -T       |--transcriptionfactor |     required    |       -      |         -       |
    | -m       |--mode                |     optional    |Methyl<br>Regular|    Methyl    |
    | -l       |--logotype            |     optional    |Shannon<br>Kullback-Liebler<br>riverlake| Kullback-Liebler |
    | -r       |--regionofinterest    |     optional    |whole_genome<br>promoter<br>uniform| whole_genome |


    * The input file is a file list with the following format:

        column1: .fa filename    
    
        column2: species
    
        column3: tissue


5. Run Animation mode


    ```
    $ MethylSeqLogo Animation -i inputfile.txt -T OCT4 -m Methyl -l Kullback-Liebler -r whole_genome
    ```


    <figure>
        <img src="https://gitlab.com/fmhsu0114/MethylSeqLogo/raw/master/Output/OCT4_riverlake.gif" width=100%>
    </figure>
 -->
